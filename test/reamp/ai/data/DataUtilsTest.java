package reamp.ai.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import junit.framework.Assert;

public class DataUtilsTest {
	
	@Test
	public void should_transform_map_data_into_csv_when_keys_are_non_existant_in_some_entries() {
		// given
		DataUtils dataUtils = DataUtils.getInstance();
		
		List<Map<String, String>> sampleData = new ArrayList<Map<String,String>>();
		HashMap<String, String> sampleItem = new HashMap<String,String>();
		
		sampleData.add(new HashMap<String,String>(){{put("ID","1");put("colunaA","x");put("colunaAA","a");put("colunaB","y");put("colunaC","z");}});
		sampleData.add(new HashMap<String,String>(){{put("ID","2");put("colunaA","x");put("colunaB","y");put("colunaC","z");}});
		sampleData.add(new HashMap<String,String>(){{put("ID","3");put("colunaA","x");put("colunaB","");put("colunaC","z");put("coluna3","4");}});
		sampleData.add(new HashMap<String,String>(){{put("ID","4");put("colunaA",null);put("colunaB","y");put("colunaC","z");}});
		sampleData.add(new HashMap<String,String>(){{put("ID","5");put("colunaB","y");put("colunaC","z");}});
		sampleData.add(new HashMap<String,String>(){{put("ID","6");}});
		
		// when
		String resultingString = dataUtils.mapToCsv(sampleData);
		
		System.out.println("--");
		System.out.println(resultingString);
		System.out.println("--");
		
		// then
		String[] lines = resultingString.split("\n");
		for (String line : lines) {
			Assert.assertEquals(4, dataUtils.countChars(';', line));
		}
	}
	
	@Test
	public void should_transform_map_data_into_csv_using_header_columns_with_one_item() {
		// given
		DataUtils dataUtils = DataUtils.getInstance();
		
		List<Map<String, String>> sampleData = new ArrayList<Map<String,String>>();
		HashMap<String, String> sampleItem = new HashMap<String,String>();
		sampleItem.put("queijo", "batata");
		sampleData.add(sampleItem);
		
		// when
		String resultingString = dataUtils.mapToCsv(sampleData);
		
		// then
		Assert.assertEquals("caraio nao ta gerando o csv certo porra!", "queijo\nbatata", resultingString);
	}
	
	@Test
	public void should_transform_map_data_into_csv_using_header_columns_with_multiple_items() {
		// given
		DataUtils dataUtils = DataUtils.getInstance();
		
		List<Map<String, String>> sampleData = new ArrayList<Map<String,String>>();
		HashMap<String, String> sampleItem = new HashMap<String,String>();
		sampleItem.put("queijo", "batata");
		sampleItem.put("jacare", "requeijao");
		sampleItem.put("trapezio", "crocodilo");
		sampleData.add(sampleItem);
		
		// when
		String resultingString = dataUtils.mapToCsv(sampleData);
		
		// then
		Assert.assertEquals("caraio nao ta gerando o csv certo porra!", "trapezio;queijo;jacare\ncrocodilo;batata;requeijao", resultingString);
	}

}
