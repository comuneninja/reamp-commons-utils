package reamp.tools.ai.mail;

import java.net.URL;
import java.net.URLDecoder;
import java.util.LinkedHashSet;
import java.util.Set;

import reamp.ai.utils.TextUtils;

public class ReampUrl {

	private String url;

	private String pureUrl;

	private String queryString;

	private int encodingAmount;

	private boolean encodingAmountIgnorable;

	private TextUtils textUtils = TextUtils.getInstance();

	private UrlUtils urlUtils = UrlUtils.getInstance();

	private Set<UrlParam> urlParamSet = new LinkedHashSet<UrlParam>();

	public ReampUrl(String url, boolean encodingAmountIgnorable) {
		super();
		this.encodingAmountIgnorable = encodingAmountIgnorable;
		if (textUtils.isEmpty(url))
			return;

		if (!encodingAmountIgnorable) {
			url = fullDecode(url);
		}

		this.url = url;

		String[] urlParts = url.split("\\?");
		if (urlParts != null && urlParts.length > 0) {
			String pureUrl = urlParts[0];
			this.pureUrl = pureUrl;
			if (urlParts.length > 1) {
				String queryString = urlParts[1];
				this.queryString = queryString;
			}
		}

		if (queryString != null) {
			String[] params = queryString.split("&");
			if (params != null && params.length > 0) {
				for (String param : params) {
					String[] paramParts = param.split("=");
					if (paramParts != null && paramParts.length > 0) {
						String parName = paramParts[0];
						String parValue = "";
						if (paramParts.length > 1) {
							parValue = paramParts[1];
						}
						UrlParam urlParam = new UrlParam(parName, parValue);
						urlParamSet.add(urlParam);
					}
				}
			}
		}
	}
	
	public String getDomainName() {
	    String host;
	    try {
			if (!url.startsWith("http") && !url.startsWith("https")) {
				url = "http://" + url;
			}
			URL netUrl = new URL(url);
			host = netUrl.getHost();
			if (host.startsWith("www")) {
				host = host.substring("www".length() + 1);
			}
			int firstDot = host.indexOf(".");
			if(firstDot <= 0) return host;
			host = host.substring(0,firstDot);
		} catch (Exception e) {
			return null;
		}
		return host;
	}

	public String fullDecode(String url) {
		try {
			String useUrl = url;
			while (!useUrl.equals((useUrl = URLDecoder.decode(useUrl))))
				encodingAmount++;
			url = useUrl;
		} catch (Exception e) {
			 
		}
		return url;
	}

	public void addUrlParam(String parName, String parValue) {
		urlParamSet.remove(new UrlParam(parName, parValue));
		urlParamSet.add(new UrlParam(parName, parValue));
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getParam(String parName) {
		for (UrlParam par : this.urlParamSet) {
			if (par.getParName().equals(parName)) {
				return par.getParValue();
			}
		}
		return null;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder(pureUrl);
		if (urlParamSet != null && urlParamSet.size() > 0)
			sb.append("?");
		int c = 0;
		for (UrlParam urlParam : urlParamSet) {
			c++;
			String parName = urlParam.getParName();
			String parValue = urlParam.getParValue();
			sb.append(parName + "=" + parValue);
			if (c < urlParamSet.size())
				sb.append("&");
		}
		return sb.toString();
	}

	public String toUrl() {
		String finalUrl = this.toString();
		if (encodingAmountIgnorable) {
			return finalUrl;
		} else {
			return urlUtils.encodeNTimes(finalUrl, encodingAmount);
		}
	}

	public String getPureUrl() {
		return pureUrl;
	}

	public void setPureUrl(String pureUrl) {
		this.pureUrl = pureUrl;
	}

	public String getQueryString() {
		return queryString;
	}

	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	public UrlUtils getUrlUtils() {
		return urlUtils;
	}

	public void setUrlUtils(UrlUtils urlUtils) {
		this.urlUtils = urlUtils;
	}

	public boolean isEncodingAmountIgnorable() {
		return encodingAmountIgnorable;
	}

	public void setEncodingAmountIgnorable(boolean encodingAmountIgnorable) {
		this.encodingAmountIgnorable = encodingAmountIgnorable;
	}

}
