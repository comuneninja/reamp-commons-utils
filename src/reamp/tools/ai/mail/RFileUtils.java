package reamp.tools.ai.mail;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;

import com.mashape.unirest.http.Unirest;

public class RFileUtils {

	private static RFileUtils instance = new RFileUtils();

	private RFileUtils() {
	}

	public static RFileUtils getInstance() {
		return instance;
	}

	public File createFile(String path, String data) {
		byte[] content = null;
		
		try {
			content = data.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return createFile(path, content);
	}
	
	public void mkdirs(String path){
		new File(path).mkdirs();
	}

	public File createFile(String path, InputStream input) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		byte[] buf = new byte[1024];
		int n = 0;

		try {
			while (-1 != (n = input.read(buf))) {
				out.write(buf, 0, n);
			}

			out.close();
			input.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		byte[] content = null;
		
		try {
			content = out.toString("UTF-8").getBytes();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return createFile(path, content);
	}

	public File createFile(String path, byte[] content) {
		try {
			File file = new File(path);
			if(file.exists())
				file.delete();
			String[] aux = path.split("/");
			String fileName = aux[aux.length -1 ];
			System.out.println("creating file: " + fileName);
			mkdirs(path.replace(fileName, ""));
			Files.write(Paths.get(path), content, StandardOpenOption.CREATE_NEW);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new File(path);
	}

	public String readFile(String filePath) throws FileNotFoundException {
		return readFile(new File(filePath));
	}
	
	public String read(InputStream input) throws FileNotFoundException {
		BufferedReader reader = null;
		StringBuilder fileString = null;

		try {
			reader = new BufferedReader(new InputStreamReader(input, "utf-8"));

			if (reader != null)
				fileString = new StringBuilder();

			String line;

			while ((line = reader.readLine()) != null) {
				fileString.append(line);
				fileString.append("\n");
			}

		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {

		} finally {
			close(reader);
		}

		try {
			return new String(fileString.toString().getBytes(), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String readFile(File file) throws FileNotFoundException {
		return this.read(new FileInputStream(file));
	}

	public BufferedReader createReader(String filePath) throws FileNotFoundException {
		BufferedReader reader = null;

		try {
			File file = new File(filePath);
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			throw e;
		}

		return reader;
	}

	public void close(BufferedReader reader) {
		try {
			reader.close();
		} catch (Exception e) {

		}
	}
	
	private Map<String,File> fileCache = new HashMap<String,File>();
	
	private Map<String,File> filePathCache = new HashMap<String,File>();

	public File downloadFile(String url, String path) throws IOException {
		File file;

		synchronized (fileCache) {
			File cachedFile = fileCache.get(url);
			if (cachedFile != null)
				return cachedFile;
			
			file = downloadThroughInputStream(url, path);
			
			if(file.exists()) {
				long fileLength = file.length();
				if((fileLength/1024) < 10*1024) {
					downloadThroughUnirest(url, path);
				}
			} else {
				downloadThroughUnirest(url, path);
			}
			
			fileCache.put(url, file);
			filePathCache.put(path, file);
		}
		
		return file;
	}

	private void downloadThroughUnirest(String url, String path) {
		try {
			String content = Unirest.get(url).asString().getBody();
			FileUtils.writeStringToFile(new File(path),content);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private File downloadThroughInputStream(String url, String path)
			throws MalformedURLException, IOException, FileNotFoundException {
		File file;
		URL link = new URL(url);
		InputStream in = new BufferedInputStream(link.openStream());
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int n = 0;
		while (-1 != (n = in.read(buf))) {
			out.write(buf, 0, n);
		}
		out.close();
		in.close();
		byte[] response = out.toByteArray();
		file = new File(path);
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(response);
		fos.close();
		return file;
	}
	
	public InputStream downloadFileInputSream(String url) throws IOException {
		URL link = new URL(url);
		return new BufferedInputStream(link.openStream());
	}

	public boolean externalFileExist(String url){
		String path = "temp/xpto";
		URL link;
		try {
			link = new URL(url);
			InputStream in = new BufferedInputStream(link.openStream());
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			
			byte[] buf = new byte[1024];
			int n = 0;
			
			while (-1 != (n = in.read(buf))) {
				out.write(buf, 0, n);
			}
			
			out.close();
			in.close();
			
			byte[] response = out.toByteArray();
			
			File file = new File(path);
			
			FileOutputStream fos = new FileOutputStream(file);
			
			fos.write(response);
			fos.close();
			
			return true;
			
		} catch (Exception e) {
			
			
		}
		return false;
	}
	
	public InputStream inputStreamFromZipFileUrl(String url) throws IOException {
		ZipInputStream zipInput = this.zipInputStreamFromUrl(url);
	    int len = 0;
	    byte data[] = new byte[1024];
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    while ((len = zipInput.read(data)) > 0) {
	        out.write(data, 0, len);
	    }
	    return new ByteArrayInputStream(out.toByteArray());
	}
	
	
	public File extractFileFromZip(String path, String data) throws IOException {
		ZipInputStream zipInput = this.zipInputStreamFromPath(path);
		ZipEntry entry = zipInput.getNextEntry();
				
		byte[] buffer = new byte[1024];
		
		String fileName = entry.getName();
		File newFile = new File(path.concat(fileName));
		
		FileOutputStream fos = new FileOutputStream(newFile);
		
		int len;
		while ((len = zipInput.read(buffer)) > 0) {
			fos.write(buffer, 0, len);
		}
		
		fos.close();
		
		zipInput.closeEntry();
		zipInput.close();
		
		return newFile;
	}
	
	
	public ZipInputStream zipInputStreamFromPath(String path) throws FileNotFoundException {
		return new ZipInputStream(new FileInputStream(path));
	}
	
	
	public ZipInputStream zipInputStreamFromUrl(String url) throws IOException {
		URL link = new URL(url);
		InputStream in = new BufferedInputStream(link.openStream());
		return new ZipInputStream(in);
	}
	
	public ZipInputStream zipInputStreamFromInputStream(InputStream in) {
		return new ZipInputStream(in);
		
	}
	
	public OutputStream extractFileFromZip(InputStream in) throws IOException {
		ZipInputStream zipInput = new ZipInputStream(in);
		zipInput.getNextEntry();
				
		byte[] buffer = new byte[1024];
		OutputStream o = new ByteArrayOutputStream();
		int len;
		while ((len = zipInput.read(buffer)) > 0) {
			o.write(buffer, 0, len);
		}
		o.close();
		
		zipInput.closeEntry();
		zipInput.close();
		
		return o;
	}

	public void shouldDelete(File tempFilePathFile) {
		String path = tempFilePathFile.getAbsolutePath();
		if(!filePathCache.containsKey(path)) tempFilePathFile.delete(); 
	}

}
