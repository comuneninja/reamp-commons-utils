package reamp.tools.ai.mail;

public class UrlParam {
	
	private String parName;
	
	private String parValue;

	public UrlParam(String parName, String parValue) {
		super();
		this.parName = parName;
		this.parValue = parValue;
	}

	public String getParName() {
		return parName;
	}

	public void setParName(String parName) {
		this.parName = parName;
	}

	public String getParValue() {
		return parValue;
	}

	public void setParValue(String parValue) {
		this.parValue = parValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((parName == null) ? 0 : parName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UrlParam other = (UrlParam) obj;
		if (parName == null) {
			if (other.parName != null)
				return false;
		} else if (!parName.equals(other.parName))
			return false;
		return true;
	}
	
}
