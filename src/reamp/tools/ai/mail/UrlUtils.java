package reamp.tools.ai.mail;

import java.io.File;
import java.io.InputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.zeroturnaround.zip.commons.FileUtils;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

import reamp.ai.utils.TextUtils;

public class UrlUtils {

	private static UrlUtils INSTANCE = null;
	
	private TextUtils textUtils = TextUtils.getInstance();

	private UrlUtils() {

	}

	public static UrlUtils getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new UrlUtils();
		}
		return INSTANCE;
	}

	private String getBaseUrl(String url) {
		String baseUrl = url;
		String workUrl = url;
		if (url.startsWith("http://")) {
			workUrl = url.replace("http://", "");
			int indexOf = workUrl.indexOf("/");
			String s = workUrl;
			if (indexOf > 0) {
				s = workUrl.substring(0, indexOf);
			}
			baseUrl = "http://" + s;
		}
		return baseUrl;
	}

	public String addPToUrl(String url, String par, String value) {
		ReampUrl reampUrl = new ReampUrl(url, true);
		
		reampUrl.addUrlParam(par, value);
		
		return reampUrl.toUrl();
	}
	
	public String decodeFull(String url) {
		return fullDecode(url);
	}
	
	public String decodeNTimes(String url, int times) {
		try {
			String useUrl = url;
			int c = 0;
			while (c++ < times) useUrl = URLDecoder.decode(useUrl, "UTF-8");
			url = useUrl;
		} catch (Exception e) {
			 
		}
		return url;
	}
	
	public String fullDecode(String url) {
		try {
			String useUrl = url;
			while (!useUrl.equals((useUrl = URLDecoder.decode(useUrl, "UTF-8"))));
			url = useUrl;
		} catch (Exception e) {
			 
		}
		return url;
	}
	
	public String extractNameFromEmail(String mail) {
		if(mail == null) return null;
		String receiverName = null;
		if(mail.contains(" ")) {
			String[] split = mail.split(" ");
			receiverName = split[0];
		} else {
			String[] split = mail.split("@");
			String namePart = split[0];
			String[] namesplit = namePart.split("\\.");
			receiverName = textUtils.camelize(namesplit[0]);
		}
		return receiverName;
	}
	
	public String encodeNTimes(String finalUrl, int amount) {
		for (int i = 0; i < amount; i++) {
			finalUrl = URLEncoder.encode(finalUrl);
		}
		return finalUrl;
	}
	
	public String downloadAsString(String url) throws Exception{
		return Unirest.get(url).asString().getBody();
	}
	
	public void downloadAndSave(String url, String filePath) throws Exception{
		HttpResponse<InputStream> asBinary = Unirest.get(url).asBinary();
		if(asBinary != null) {
			FileUtils.copy(asBinary.getBody(), new File(filePath));
		}
	}
}
