package reamp.ai.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class DbUtils {
	
	public static final String VITRINES_DBNAME = "vitrines";
	
	public static final String RMC_DBNAME = "reamp_adserver_dashboard";

	public static final String DASH_DBNAME = "dash";

	private static DbUtils instance;
	
	private Map<String,Connection> connectionsMap = new HashMap<String,Connection>();

	private DbUtils() {
	}

	public static DbUtils getInstance() {
		if (null == instance) {
			instance = new DbUtils();
		}
		return instance;
	}
	
	public Connection connect(String name) {
		Connection c = connectionsMap.get(name);
		if(c != null) return c;
		if(DASH_DBNAME.equals(name)) {
			c = connectDash();
		} else if (VITRINES_DBNAME.equals(name)) {
			c = connectVitrines();
		} else if (RMC_DBNAME.equals(name)) {
			c = connectRMC();
		}
		return c;
	}
	
	public Connection connectVitrines() {
		Connection connect = null;
		try {
			System.out.println(">>>> connecting to vdb.reamp.com.br");
			connect = DriverManager.getConnection("jdbc:mysql://vdb.reamp.com.br:3306/vitrines?" + "user=root&password=VFR$5tgb");
			System.out.println(">>>> connected");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connect;
	}
	
	public Connection connectRMC() {
		Connection connect = null;
		try {
			System.out.println(">>>> connecting to rmcdb.reamp.com.br");
			connect = DriverManager.getConnection("jdbc:mysql://rmcdb.reamp.com.br:3306/reamp_adserver_dashboard?" + "user=admin_adserver&password=6dkJHLGAbFDHRGWFABxHJHav8pskPy");
			System.out.println(">>>> connected");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connect;
	}
	
	public Connection connectDash() {
		Connection connect = null;
		try {
			System.out.println(">>>> connecting to dashdb.reamp.com.br");
			connect = DriverManager.getConnection("jdbc:mysql://dashdb.reamp.com.br:3306/reamp_dashboard?" + "user=comune&password=azXSdc78");
			System.out.println(">>>> connected");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connect;
	}
	
	public void insertInto(Connection connect, String tableName, Map<String,String> obj) {
		StringBuilder fullQuery = new StringBuilder();
		
		StringBuilder firstPartSb = new StringBuilder();
		firstPartSb.append("insert into " + tableName);
		firstPartSb.append("(");
		
		StringBuilder valuesPartSb = new StringBuilder();
		valuesPartSb.append(" values (");
		
		for (Entry<String, String> entry : obj.entrySet()) {
			String key = entry.getKey();
			firstPartSb.append("`" + key + "`,");
			valuesPartSb.append("'" + obj.get(key) + "',");
		}
		firstPartSb.append(")");
		valuesPartSb.append(")");
		
		String firstPart = firstPartSb.toString().replaceAll(",\\)", ")");
		String valuesPart = valuesPartSb.toString().replaceAll(",\\)", ")");
		
		fullQuery.append(firstPart);
		fullQuery.append(valuesPart);
		
		try {
			PreparedStatement preparedStatement = null;
			preparedStatement = connect.prepareStatement(fullQuery.toString(), Statement.RETURN_GENERATED_KEYS);
			preparedStatement.executeUpdate();
			ResultSet rs = preparedStatement.getGeneratedKeys();
			rs.next();
		} catch (Exception e) {
			System.out.println("could not insert into " + tableName + ":" + e.getMessage());
		}
	}
	
	public Map<String, String> firstFromSql(Connection connect, String campaignsLinkedToZoneInVitrines) {
		List<Map<String, String>> list = listMapFromSql(connect, campaignsLinkedToZoneInVitrines);
		if(list == null || list.size() == 0) return null;
		return list.get(0);
	}
	
	public List<Map<String, String>> listMapFromSql(Connection connect, String campaignsLinkedToZoneInVitrines) {
		List<Map<String,String>> resultMap = new ArrayList<Map<String,String>>();
		try {
			
			ResultSet result = connect.createStatement().executeQuery(
					campaignsLinkedToZoneInVitrines);
			
			List<String> labels = new ArrayList<String>();
			ResultSetMetaData metaData = result.getMetaData();
			int columnCount = metaData.getColumnCount();
			for (int i = 1; i <= columnCount; i++) {
				String columnLabel = metaData.getColumnName(i);
				labels.add(columnLabel);
			}
			
			while (result.next()) {
				Map<String,String> el = new HashMap<String,String>();
				if(labels != null && labels.size() > 0) {
					for (String label : labels) {
						String valueAsString = result.getString(label);
						el.put(label, valueAsString);
					}
				}
				resultMap.add(el);
			} 
		} catch (Exception e) {
			e.printStackTrace(); 
		}
		return resultMap;
	}

}
