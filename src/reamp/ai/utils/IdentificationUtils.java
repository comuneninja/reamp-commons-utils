package reamp.ai.utils;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Random;

public class IdentificationUtils {
	
	private static final Random RAND = new Random();
	
	private static final IdentificationUtils singleton = new IdentificationUtils();
	
	private TextUtils textUtils = TextUtils.getInstance();

	public static IdentificationUtils getInstance() {
		return singleton;
	}

	private IdentificationUtils() {
	}
	
	public String id(String name) {
		String random = new DecimalFormat("000000").format(RAND.nextInt(1000000));
		String dat = Long.toString(new Date().getTime());
		if(name == null)
			return dat + "-" + random;
		if(name.equals(""))
			return dat + "-" + random; 
		String string = name + "-" + dat + "-" + random;
		return textUtils.md5(string);
	}
}
