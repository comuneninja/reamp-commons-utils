package reamp.ai.utils;

import java.util.HashSet;
import java.util.Set;

public class ValuePair<V1,V2> {
	
	private V1 key;
	
	private V2 value;

	public ValuePair(V1 key, V2 value) {
		super();
		this.key = key;
		this.value = value;
	}

	public V1 getKey() {
		return key;
	}

	public void setKey(V1 key) {
		this.key = key;
	}

	public V2 getValue() {
		return value;
	}

	public void setValue(V2 value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return -1;
	}
	
	@Override
	public boolean equals(Object obj) {
		ValuePair other = (ValuePair) obj;
		Set proveSet = new HashSet();
		proveSet.add(key);
		proveSet.add(value);
		return (proveSet.contains(other.key) && proveSet.contains(other.value));
	}

	@Override
	public String toString() {
		return "ValuePair [key=" + key + ", value=" + value + "]";
	}

}
