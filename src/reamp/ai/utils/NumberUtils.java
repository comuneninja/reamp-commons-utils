package reamp.ai.utils;

public class NumberUtils {

	private static final NumberUtils singleton = new NumberUtils();

	private NumberUtils() {
	}

	public static NumberUtils getInstance() {
		return singleton;
	}

	public Number toNumber(String o) {
		Number n = null;
		try {
			n = Float.parseFloat(o);
			if (n != null)
				return n;
		} catch (Exception e) {
			// 
		}
		
		try {
			n = Integer.parseInt(o);
			if (n != null)
				return n;
		} catch (Exception e) {
			// 
		}
		return n;
	}
	
	public Float parseForFloatValue(String value) throws Exception {
		try {
			float parseFloat = Float.parseFloat(value);
			return parseFloat;
		} catch (Exception e) {
			try {
				// check if it has both commas and dots
				int comma = value.indexOf(",");
				int dot = value.indexOf(".");
				if (comma != -1 && dot != -1) {
					if (comma < dot) {
						String before = value.substring(0, comma);
						String after = value.substring(comma + 1);
						value = before + after;
					} else {
						String before = value.substring(0, dot);
						String after = value.substring(dot + 1);
						value = before + after;
					}
				} else {
					// check for multiple commas or dots
					if (comma != value.lastIndexOf(",")) {
						String before = value.substring(0, comma);
						String after = value.substring(comma + 1);
						value = before + after;
					}
					if (dot != value.lastIndexOf(".")) {
						String before = value.substring(0, dot);
						String after = value.substring(dot + 1);
						value = before + after;
					}
				}

				// replace commas and dots
				value = value.replaceAll(",", ".");
				float parseFloat = Float.parseFloat(value);
				return parseFloat;
			} catch (Exception e2) {
				try {
					int parseInt = Integer.parseInt(value);
					return (float) parseInt;
				} catch (Exception e3) {
					return null;
				}
			}
		}
	}

}
