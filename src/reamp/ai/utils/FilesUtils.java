package reamp.ai.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class FilesUtils {

	private static final FilesUtils singleton = new FilesUtils();

	private FilesUtils() {
	}

	public static FilesUtils getInstance() {
		return singleton;
	}

	public void getFilesFromS3DirectoryToLocal(String bucketName, String directory, String localPath) {

		ClientConfiguration clientConfig = new ClientConfiguration();
		clientConfig.setProtocol(Protocol.HTTP);
		AmazonS3Client s3Client = new AmazonS3Client(getCredentials(), clientConfig);

		List<String> fileList = listFilesS3Directory(bucketName, directory, s3Client);
		for (String filepath : fileList) {
			String file = filepath.substring(filepath.lastIndexOf('/') + 1, filepath.length());
			saveFileFromS3(bucketName, filepath, localPath + "/" + file, s3Client);
		}
		return;
	}

	public List<String> listFilesS3Directory(String bucketName, String directory) {
		ClientConfiguration clientConfig = new ClientConfiguration();
		clientConfig.setProtocol(Protocol.HTTP);
		AmazonS3Client s3Client = new AmazonS3Client(getCredentials(), clientConfig);
		return listFilesS3Directory(bucketName, directory, s3Client);
	}

	public List<String> listFilesS3Directory(String bucketName, String directory, AmazonS3Client s3Client) {
		List<String> fileList = new ArrayList<String>();
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName)
				.withPrefix(directory);
		ObjectListing objectListing = s3Client.listObjects(listObjectsRequest);
		for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
			fileList.add(objectSummary.getKey());
		}
		return fileList;
	}

	public void saveFileFromS3(String bucketName, String fileName, String localPath) {
		ClientConfiguration clientConfig = new ClientConfiguration();
		clientConfig.setProtocol(Protocol.HTTP);
		AmazonS3Client s3Client = new AmazonS3Client(getCredentials(), clientConfig);
		saveFileFromS3(bucketName, fileName, localPath, s3Client);
	}

	public void saveFileFromS3(String bucketName, String fileName, String localPath, AmazonS3Client s3Client) {
		S3Object fetchFile = s3Client.getObject(new GetObjectRequest(bucketName, fileName));
		InputStream objectData = fetchFile.getObjectContent();
		try {
			Files.copy(objectData, new File(localPath).toPath());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				objectData.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public BasicAWSCredentials getCredentials() {

		Properties properties = new Properties();
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream is = classloader.getResourceAsStream("aws-credentials.properties");
		try {
			properties.load(is);
		} catch (IOException e) {
			System.out.println("Configure as credenciais do s3");
		}

		String accessKey = properties.getProperty("access_key");
		String secretKey = properties.getProperty("secret_key");

		return new BasicAWSCredentials(accessKey, secretKey);
	}

	public void zipFiles(List<File> inputFileList, String outputDirFile) {
		ZipOutputStream zip = null;
		try {
			zip = new ZipOutputStream(new FileOutputStream(outputDirFile));

			for (File f : inputFileList) {
				String fileName = f.getName();
				zip.putNextEntry(new ZipEntry(fileName));
				BufferedInputStream bis = new BufferedInputStream(new FileInputStream(f));
				int b = bis.read();
				while (b >= 0) {
					zip.write(b);
					b = bis.read();
				}
				bis.close();
				zip.closeEntry();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				zip.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
