package reamp.ai.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import reamp.commons.util.CounterHandler;
import reamp.indexer.DateUtils;

public class ValueUtils {

	private static final ValueUtils singleton = new ValueUtils();

	private TextUtils textUtils = TextUtils.getInstance();
	private DateUtils dateUtils = DateUtils.getInstance();

	static RandomUtil rand = RandomUtil.getInstance();

	private Map<String, CounterHandler> mc = new HashMap<String, CounterHandler>();

	private static Map<String, String> publisherNameMap;
	private static Map<String, String> advertiserNameMap;

	Map<String, Map<String, Float>> advertiserColumns = new HashMap<String, Map<String, Float>>();
	Map<String, Map<String, Float>> advertiserNameColumns = new HashMap<String, Map<String, Float>>();

	List<String> fields = new ArrayList<String>();

	static boolean production = true;

	static int attemptLimit = 3;

	public Integer forceParseAsInt(String s) {
		try {
			int parseInt = Integer.parseInt(s);
			return parseInt;
		} catch (Exception e) {
			try {
				Float f = Float.parseFloat(s);
				return f.intValue();
			} catch (Exception e2) {
			}
		}
		return null;
	}

	public Float forceParseAsFloat(String s) {
		try {
			float parseInt = Float.parseFloat(s);
			return parseInt;
		} catch (Exception e) {
			try {
				Integer f = Integer.parseInt(s);
				return f.floatValue();
			} catch (Exception e2) {
			}
		}
		return null;
	}

	String IPADDRESS_PATTERN = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";

	public boolean isIP(String ip) {

		Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
		Matcher matcher = pattern.matcher(ip);

		if (matcher.find()) {
			return true;
		} else {
			return false;
		}
	}

	public Double forceParseAsDouble(String s) {
		try {
			double parseInt = Double.parseDouble(s);
			return parseInt;
		} catch (Exception e) {
			try {
				Integer f = Integer.parseInt(s);
				return f.doubleValue();
			} catch (Exception e2) {
			}
		}
		return null;
	}

	private ValueUtils() {
	}

	public static ValueUtils getInstance() {
		return singleton;
	}

	public Date parseAsDate(String token) {
		return dateUtils.parseFlexibly(token);
	}

	public String findAsAdvertiser(String token) {
		return advertiserNameMap.get(token);
	}

	public boolean isImageResource(String value) {
		return textUtils.endsWithIgnoreCase(value, ".gif", ".jpeg", ".jpg", ".png");
	}

	public boolean isSwfResource(String value) {
		return textUtils.endsWithIgnoreCase(value, ".swf");
	}

	public Long forceParseAsLong(String s) {
		try {
			long parseLong = Long.parseLong(s);
			return parseLong;
		} catch (Exception e) {
			try {
				Float f = Float.parseFloat(s);
				return f.longValue();
			} catch (Exception e2) {
			}
		}
		return null;
	}

}
