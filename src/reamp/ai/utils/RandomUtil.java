package reamp.ai.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class RandomUtil {

	private static RandomUtil INSTANCE = null;

	private RandomUtil() {

	}

	public static RandomUtil getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new RandomUtil();
		}
		return INSTANCE;
	}

	public double random(double min, double max) {
		return min + Math.random() * (max - min);
	}

	public boolean applyChance(double chance) {
		double r = random(0, 100);
		return r < chance;
	}

	public <T> T getRandomically(List<T> list) {
		if (list.size() == 0)
			return null;
		return list.get((int) random(0, list.size() - 1));
	}

	public List<String> randomList(List<String> advertisers, int size) {
		RandomUtil rand = RandomUtil.getInstance();
		List<String> nl = new ArrayList<String>();
		Set<String> set = new HashSet<String>();
		for (int i = 0; i < size; i++) {
			String randItem = rand.getRandomically(advertisers);
			if (set.add(randItem)) {
				nl.add(randItem);
			}
			;
		}
		return nl;
	}

	public <K, V> K getKeyRandomically(Map<K, V> list) {
		Set<K> keySet = list.keySet();
		int i = 0;
		int r = (int) random(0, keySet.size() - 1);
		for (K k : keySet) {
			if (i == r)
				return k;
			i++;
		}
		return null;
	}

	public String chooseAnother(String r, String c, double d) {
		if(r.equals(c)) return c;
		if (Math.random() < d)
			return r;
		return c;
	}

	public boolean applyChanceIfContains(String rPub, String condition, double d) {
		if(!rPub.contains(condition)) return false;
		return applyChance(d);
	}
	
	public boolean applyChanceIfEndsWith(String rPub, String condition, double d) {
		if(!rPub.endsWith(condition)) return false;
		return applyChance(d);
	}

	public String getRandomically(List<String> asList, List<String> usedPhrases) {
		int c = 3;
		for (int i = 0; i < c; i++) {
			String s = getRandomically(asList);
			if(!usedPhrases.contains(s)) {
				usedPhrases.add(s);
				return s;
			}
		}
		return getRandomically(asList);
	}
	
	public String generateString() {
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }

}
