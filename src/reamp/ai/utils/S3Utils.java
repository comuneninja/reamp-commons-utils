package reamp.ai.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

public class S3Utils {

	private final String accessKey = "AKIAJ42CUV2S7TANEYKA";
	private final String secretKey = "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";

	private AmazonS3 s3Client;

	private ClientConfiguration clientConfig;
	
	private TextUtils textUtils = TextUtils.getInstance();

	private static final S3Utils singleton = new S3Utils();

	private S3Utils() {
		ClientConfiguration clientConfig = new ClientConfiguration();
		clientConfig.setMaxConnections(Integer.MAX_VALUE);
		clientConfig.setSocketTimeout(100000);
		clientConfig.setConnectionTimeout(100000);
		clientConfig.setProtocol(Protocol.HTTP);

		this.clientConfig = clientConfig;
		final AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		s3Client = new AmazonS3Client(credentials, clientConfig);
	}

	public static S3Utils getInstance() {
		return singleton;
	}

	public String putObject(File f, String key) {
		return putObject(f, key, "application/json");
	}
	
	public String putObject(File f, String key, String contentType) {
		String contentDisposition = "inline";
		return putObject(f, key, contentType, contentDisposition);
	}

	public String putObject(File f, String key, String contentType, String contentDisposition) {
		String bucketName = "reamp-media";
		return putObject(f, key, contentType, contentDisposition, bucketName);
	}
	
	public String putObject(String data, String key, String contentType, String contentDisposition, String bucketName) throws Exception {
		File tmpFile = new File(textUtils.md5(Math.random()*9999 + "--" + System.currentTimeMillis() + key) + key.substring(key.lastIndexOf("."),key.length()));
		FileUtils.writeStringToFile(tmpFile, data);
		String putObject = putObject(tmpFile, key, contentType, contentDisposition, bucketName);
		tmpFile.delete();
		return putObject;
	}

	public String putObject(File f, String key, String contentType, String contentDisposition, String bucketName) {
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, f);
		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentType(contentType); 
		if(contentDisposition != null) meta.setContentDisposition(contentDisposition);
		putObjectRequest.withMetadata(meta);
		s3Client.putObject(putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead));
		String resourceLocation = "http://" + bucketName + ".s3.amazonaws.com" + (key.startsWith("/") ? "" : "/") + key;
		System.out.println(resourceLocation);
		return resourceLocation;
	}
	
	public String putNormalObject(File f, String key){
		String bucketName = "reamp-media";
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, f);
		
		s3Client.putObject(putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead));		
		String resourceLocation = "http://" + bucketName + ".s3.amazonaws.com" + "/" + key;
		return resourceLocation;
	}
	
	public String putPrivateObject(File f, String key){
		String bucketName = "reamp-media";
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, f);
		
		s3Client.putObject(putObjectRequest.withCannedAcl(CannedAccessControlList.Private));		
		String resourceLocation = "http://" + bucketName + ".s3.amazonaws.com" + "/" + key;
		return resourceLocation;
	}
	
	public String putObject(String s, String key) {
		String tempFileName = new Date().getTime() + "-" + ((int)Math.floor(Math.random()*10000)) + "-tmp";
		File tempFile = new File(tempFileName);
		tempFile.getAbsolutePath();
		try {
			FileUtils.writeStringToFile(tempFile, s);
		} catch (Exception e) {
		}
		
		String putObject = putObject(tempFile, key);
		try {
			tempFile.delete();
		} catch (Exception e) {
		}
		return putObject;
	}

	public void downloadFile(String url, String fileName) throws IOException {
		URL link = new URL(url); 
		InputStream in = new BufferedInputStream(link.openStream());
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int n = 0;
		while (-1 != (n = in.read(buf))) {
			out.write(buf, 0, n);
		}
		out.close();
		in.close();
		byte[] response = out.toByteArray();

		FileOutputStream fos = new FileOutputStream(fileName);
		fos.write(response);
		fos.close();
		System.out.println("Finished");

	}

}
