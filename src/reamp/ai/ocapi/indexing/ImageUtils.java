package reamp.ai.ocapi.indexing;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.mashape.unirest.http.Unirest;
import com.tinify.Options;
import com.tinify.Source;
import com.tinify.Tinify;

import reamp.ai.data.file.ResourceHandler;
import reamp.ai.utils.TextUtils;

public class ImageUtils {

	private static ImageUtils instance;

	private ResourceHandler resourceHandler = ResourceHandler.getInstance();
	
	private TextUtils textUtils = TextUtils.getInstance();

	private Map<String, BufferedImage> imageCache = new HashMap<String, BufferedImage>();

	private ImageUtils() {
	}

	public static ImageUtils getInstance() {
		if (null == instance) {
			instance = new ImageUtils();
		}
		return instance;
	}

	public BufferedImage imageFromResource(String resource) throws Exception {
		BufferedImage imageFromCache = imageCache.get(resource);
		if (imageFromCache != null)
			return imageFromCache;
		InputStream inputStream = null;
		if (resource.startsWith("file://")) {
			resource = resource.replaceAll("file://", "");
			inputStream = new FileInputStream(resource);
		} else {
			inputStream = Unirest.get(resource).asBinary().getBody();
		}
		BufferedImage in = ImageIO.read(inputStream);
		imageCache.put(resource, in);
		return in;
	}

	public BufferedImage resizeToWidth(String image, Number newW) throws Exception {
		BufferedImage imageFromResource = loadImage(image);
		Number width = imageFromResource.getWidth();
		Number height = imageFromResource.getHeight();
		Number newH = height.floatValue() / (width.floatValue() / newW.floatValue());
		BufferedImage resize = resize(image, newW.intValue(), newH.intValue());
		imageToFile(resize, new File("resources/images/newsize.png"));
		return resize;
	}

	public BufferedImage resize(String image, int newW, int newH) throws Exception {
		BufferedImage imageFromResource = loadImage(image);
		return resize(imageFromResource, newW, newH);
	}

	private BufferedImage loadImage(String image) throws Exception {
		BufferedImage imageFromResource;
		try {
			imageFromResource = imageFromResource(image);
		} catch (Exception e) {
			throw new Exception("could not load resource at " + image);
		}
		return imageFromResource;
	}
	
	static {
		Tinify.setKey("if6jWDaWTeq5ZZOTCC4paXWzn8KAeHOi");
	}
	
	public String rescaleToWidth(String originalUrl, int newW) {
		System.out.println("downloading " + originalUrl);
		
		Source source = Tinify.fromUrl(originalUrl);
		String bucket = "reamp-assets";
		String imagePath = "/thumbs/" + textUtils.md5(originalUrl + "1") + ".png";
		String rescaledUrl = "https://rat.vitrines.in" + imagePath;
		
		boolean resourceExists = false;
		try {
			resourceExists = resourceHandler.checkResource(rescaledUrl);
			System.out.println("url already on " + rescaledUrl);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String path = bucket + imagePath;
		if(!resourceExists) {
			Options options = new Options()
				    .with("method", "scale")
				    .with("width", newW);
			Source resize = source.resize(options);
			options = new Options()
					.with("service", "s3")
				    .with("aws_access_key_id", "AKIAJ5VN2XK2MB7D7BWA")
				    .with("aws_secret_access_key", "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")
				    .with("region", "us-east-1")
				    .with("path", path);
			resize.store(options);
		}
		
		System.out.println("image now in " + rescaledUrl);
		return rescaledUrl;
	}

	public BufferedImage resize(BufferedImage img, int newW, int newH) {
		int w = img.getWidth();
		int h = img.getHeight();
		BufferedImage dimg = new BufferedImage(newW, newH, img.getType());
		Graphics2D g = dimg.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(img, 0, 0, newW, newH, 0, 0, w, h, null);
		g.dispose();
		return dimg;
	}

	public static void imageToFile(BufferedImage image, File destinyFile) {
		String fileName = destinyFile.getName();
		String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
		try {
			ImageIO.write(image, extension, destinyFile);
		} catch (Exception e) {
			System.out.println("Exception occured :" + e.getMessage());
		}
		System.out.println("Images were written succesfully.");
	}

	public static void main(String[] args) throws Exception {
		ImageUtils imageUtils = ImageUtils.getInstance();
//		imageUtils.resizeToWidth("file://resources/images/print.png", 400);
		String rescaleToWidth = imageUtils.rescaleToWidth("http://reamp-media.s3.amazonaws.com/logoglobo.png", 80);
		System.out.println(rescaleToWidth);
	}

}
