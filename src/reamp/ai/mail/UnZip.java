package reamp.ai.mail;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnZip {
	
	private static final int BUFFER_SIZE = 4096;
	
	private static UnZip instance;

	private UnZip() {
	}

	public static UnZip getInstance() {
		if (null == instance) {
			instance = new UnZip();
		}
		return instance;
	}
	
	public static void main(String[] args) throws IOException {
		UnZip z = new UnZip();
		z.unZipIt("/Users/jessica/temp/mail/jarvis.zip", "/Users/jessica/temp/mail/jarvis.zip-tratra");
		
	}

	/**
	 * Unzip it
	 * 
	 * @param zipFile
	 *            input zip file
	 * @param output
	 *            zip file output folder
	 */
	public void unZipIt(String zipFilePath, String destDirectory) throws IOException {
		File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                try {
					// if the entry is a file, extracts it
					extractFile(zipIn, filePath);
				} catch (Exception e) {
				}
            } else {
                try {
					// if the entry is a directory, make the directory
					File dir = new File(filePath);
					dir.mkdir();
				} catch (Exception e) {
				}
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
	}
	
	private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }
}