package reamp.ai.maintainer.util;

public class CommandResult {
	private String message;
	
	private String errorMessage;
	
	private CommandStatus status;

	public CommandResult() {
		super();
	}

	public CommandResult(String message, String errorMessage) {
		super();
		this.message = message;
		this.errorMessage = errorMessage;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public CommandStatus getStatus() {
		return status;
	}

	public void setStatus(CommandStatus status) {
		this.status = status;
	}
}