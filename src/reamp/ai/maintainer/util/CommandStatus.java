package reamp.ai.maintainer.util;

public enum CommandStatus {
	SUCCEEDED, COMMAND_RUN, PENDING;
}