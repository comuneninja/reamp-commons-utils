package reamp.ai.maintainer.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class CommandLineUtils {
	
	private static CommandLineUtils instance;

	private CommandLineUtils() {
	}

	public static CommandLineUtils getInstance() {
		if (null == instance) {
			instance = new CommandLineUtils();
		}
		return instance;
	}
	
	public CommandResult runCommand(String commandStr) throws Exception {
		CommandResult result = new CommandResult();
		
		Runtime rt = Runtime.getRuntime();
		String[] commands = {commandStr};
		Process proc = rt.exec(commands);

		BufferedReader stdInput = new BufferedReader(new 
		     InputStreamReader(proc.getInputStream()));

		BufferedReader stdError = new BufferedReader(new 
		     InputStreamReader(proc.getErrorStream()));
		
		StringBuilder sb = new StringBuilder();
		String s = null;
		while ((s = stdInput.readLine()) != null) {
		    sb.append(s);
		}
		String message = sb.toString();

		// read any errors from the attempted command
		StringBuilder sbe = new StringBuilder();
		while ((s = stdError.readLine()) != null) {
		    sbe.append(s);
		}
		String errorMessage = sbe.toString();
		
		result = new CommandResult(message, errorMessage);
		result.setStatus(CommandStatus.SUCCEEDED);
		
		return result;
	}
	
	public Map<String, String> paramMapFromArgs(String[] args) {
		Map<String, String> paramMap = new HashMap<String, String>();
		for (int i = 0; i < args.length; i++) {
			String a = args[i];
			if (a.startsWith("-")) {
				String fieldName = a.substring(1, a.length());
				String value = args[++i];
				System.out.println(fieldName + ":" + value);
				paramMap.put(fieldName, value);
			}
		}
		return paramMap;
	}

	public String fromParamOr(Map<String, String> params, String string, String s) {
		String v = params.get(string);
		if(v == null) return s;
		return v;
	}

}
