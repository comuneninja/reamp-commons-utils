package reamp.ai.data;

import java.util.Map;

public interface MapFilter {

	boolean apply(Map<String, String> map);

}
