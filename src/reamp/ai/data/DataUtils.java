package reamp.ai.data;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import reamp.ai.utils.TextUtils;

public class DataUtils {

	private static DataUtils instance;

	private TextUtils textUtils = TextUtils.getInstance();

	private DataUtils() {
	}

	public static DataUtils getInstance() {
		if (null == instance) {
			instance = new DataUtils();
		}
		return instance;
	}

	public int countChars(char c, String s) {
		int count = 0;

		// Counts each character except space
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == c)
				count++;
		}

		return count;
	}
	
	public static String homeDir() {
		return System.getProperty("user.home") + "/.reamp";
	}

	public List<Map<String, String>> readCsv(String path) throws Exception {
		List<String> lines = FileUtils.readLines(new File(path));
		String headerLine = lines.get(0);

		Map<Integer, String> headerNameMap = new HashMap<Integer, String>();
		String[] headerFields = headerLine.split(",");
		for (int i = 0; i < headerFields.length; i++) {
			String fieldName = headerFields[i];
			headerNameMap.put(i, textUtils.coolClean(fieldName));
		}
		
		List<Map<String,String>> csvContent = new ArrayList<Map<String,String>>();
		for (int i = 1; i < lines.size(); i++) {
			Map<String,String> lineMap = new HashMap<String,String>();
			
			String line = lines.get(i);
			String[] columns = line.split(",");
			for (int j = 0; j < columns.length; j++) {
				String columnValue = columns[j];
				String header = headerNameMap.get(j);
				lineMap.put(header, columnValue);
			}
			
			csvContent.add(lineMap);
		}
		
		return csvContent;
	}

	public String mapToCsv(List<Map<String, String>> extractedData) {
		if (extractedData == null || extractedData.isEmpty())
			return "";
		Map<String, String> firstElement = extractedData.get(0);
		Set<String> firstKeySet = firstElement.keySet();

		String valueSeparator = ";";
		String lineSeparator = "\n";

		String headerColumn = StringUtils.join(firstKeySet, valueSeparator);

		List<String> linesStrList = new ArrayList<String>();
		linesStrList.add(headerColumn);

		for (Map<String, String> line : extractedData) {
			List<String> valueStrList = new ArrayList<String>();
			for (String key : firstKeySet) {
				String value = line.get(key);

				if (value != null) {
					value = value.replaceAll(",", "");
					value = value.replaceAll(";", "");
				}

				valueStrList.add(value);
			}
			String lineStr = StringUtils.join(valueStrList.toArray(), valueSeparator);
			linesStrList.add(lineStr);
		}
		String allLinesStr = StringUtils.join(linesStrList.toArray(), lineSeparator);
		return allLinesStr;
	}

	public <K,V> String printMap(Map<K, V> map) {
		StringBuilder sb = new StringBuilder();

		for (Entry<K, V> entry : map.entrySet()) {
			K key = entry.getKey();
			V value = entry.getValue();

			sb.append(key + ":" + value + ";");
		}

		return sb.toString();
	}

	public <K, V> String printData(List<Map<K, V>> data) {
		if(data == null) return null;
		StringBuilder sb = new StringBuilder();
		for (Map<K, V> dataLine : data) {
			sb.append(printMap(dataLine) + "\n");
		}
		return sb.toString();
	}

	public <K, V> Map<K, V> clone(Map<K, V> original) {
		Map<K, V> copy = new HashMap<>();

		for (Map.Entry<K, V> entry : original.entrySet()) {
			copy.put(entry.getKey(), entry.getValue());
		}

		return copy;
	}
	
	public List<Map<String, String>> cleanDataset(List<Map<String, String>> tableFromXls) {
		return cleanMaps(tableFromXls, null);
	}
	
	public boolean isURL(String text) {
		if(text == null) return false;
		return text.trim().startsWith("http");
	}
	
	public boolean isDocsURL(String text) {
		return isURL(text) && (text.contains("docs.google") || text.contains("drive.google"));
	}

	public List<Map<String, String>> cleanMaps(List<Map<String, String>> tableFromXls, MapOperator<String,String> mapOperator) {
		List<Map<String, String>> cleanTable = new ArrayList<Map<String, String>>();

		for (Map<String, String> tableElement : tableFromXls) {
			Map<String, String> cleanMap = new HashMap<String, String>();

			for (Entry<String, String> entry : tableElement.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();

				if (textUtils.isEmpty(key.trim()))
					continue;
				if (textUtils.isEmpty(value))
					continue;

				value = value.replaceAll(",", "");
				value = value.replaceAll(";", "");

				key = textUtils.coolClean(key).toLowerCase();
				
				cleanMap.put(key, value);
			}
			
			if(mapOperator != null) {
				mapOperator.cleanMap(cleanMap);
			}
			
			cleanTable.add(cleanMap);
		}
		return cleanTable;
	}

	public List<Map<String, String>> uniqueEntriesBy(List<Map<String, String>> mapList, String... fields) {
		Map<String, Map<String, String>> tempMap = new HashMap<String, Map<String, String>>();
		for (Map<String, String> map : mapList) {
			List<String> keys = new ArrayList<String>();
			for (String field : fields) {
				String fieldValue = map.get(field);
				keys.add(fieldValue);
			}
			String joinedKey = StringUtils.join(keys, "_");
			tempMap.put(joinedKey, map);
		}

		Set<Map<String, String>> resultSet = new LinkedHashSet<Map<String, String>>();
		for (Entry<String, Map<String, String>> entry : tempMap.entrySet()) {
			Map<String, String> value = entry.getValue();
			resultSet.add(value);
		}

		List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();
		for (Map<String, String> map : resultSet) {
			resultList.add(map);
		}
		return resultList;
	}

	public List<Map<String, String>> filterMapList(List<Map<String, String>> mapList, String... fields) {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		for (Map<String, String> map : mapList) {
			Map<String, String> nmap = filterMap(map, fields);
			list.add(nmap);
		}
		return list;
	}

	public boolean containsAny(String base, String... contains) {
		for (String contain : contains) {
			if (base.contains(contain))
				return true;
		}
		return false;
	}

	public List<Map<String, String>> filterMapListWithFilter(List<Map<String, String>> mapList, MapFilter... filters) {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();

		maplistloop: for (Map<String, String> map : mapList) {
			for (MapFilter entryFilter : filters) {
				if (!entryFilter.apply(map))
					continue maplistloop;
			}
			list.add(map);
		}

		return list;
	}

	public Map<String, String> filterMap(Map<String, String> map, String... fields) {
		Map<String, String> filteredMap = new HashMap<String, String>();
		for (String field : fields) {
			String value = map.get(field);
			filteredMap.put(field, value);
		}
		return filteredMap;
	}

	public List<String> trimList(List<String> list) {
		List<String> r = new ArrayList<String>();
		for (String el : list) {
			r.add(el.trim());
		}
		return r;
	}

	public <K, V> void addAllIfNotContains(Map<K, V> fromMap, Map<K, V> destinyMap) {
		for (Entry<K, V> entry : fromMap.entrySet()) {
			K key = entry.getKey();
			V value = entry.getValue();
			if (destinyMap.containsKey(key))
				continue;
			destinyMap.put(key, value);
		}
	}

	public List<Map<String, String>> expandTableWithSynonymMap(List<Map<String, String>> tableFromXls,
			Map<String, List<String>> synonymMap) {
		for (Map<String, String> map : tableFromXls) {

			for (Entry<String, List<java.lang.String>> entry : synonymMap.entrySet()) {
				String key = entry.getKey();
				List<java.lang.String> synonyms = entry.getValue();

				String valueInMap = map.get(key);
				for (String synonym : synonyms) {
					map.put(synonym, valueInMap);
				}
			}
		}
		return tableFromXls;
	}

	public List<Map<String, String>> fillEmptyEntries(List<Map<String, String>> data, String emptyPlaceholder,
			String... mandatoryFields) {
		for (Map<String, String> map : data) {
			if (mandatoryFields != null) {
				for (String field : mandatoryFields) {
					String value = map.get(field);
					if (textUtils.looksEmpty(value)) {
						map.put(field, emptyPlaceholder);
					}
				}
			}
		}
		return data;
	}

	public List<Map<String, String>> fillLeftOn(List<Map<String, String>> leftTable, List<Map<String, String>> rightTable,
			String fieldCriteria, String addPrefix) {
		
		if(leftTable == null) return null; 
		
		String fieldLeft = fieldCriteria;
		String fieldRight = fieldCriteria;
		if(fieldCriteria != null && fieldCriteria.contains(":")) {
			fieldLeft = fieldCriteria.split(":")[0];
			fieldRight = fieldCriteria.split(":")[1];
		}
		
		for (Map<String,String> leftTableElement : leftTable) {
			String leftFieldValue = leftTableElement.get(fieldLeft);
			for (Map<String, String> rightTableElement : rightTable) {
				String rightFieldValue = rightTableElement.get(fieldRight);
				if(leftFieldValue.equals(rightFieldValue)) {
					
					for (Entry<String, String> entry : rightTableElement.entrySet()) {
						String key = entry.getKey();
						String value = entry.getValue();
						
						leftTableElement.put(addPrefix + key, value);
					}
				}
			}
		}
		
		return leftTable;
	}
	
	public String googleDriveUrl(String docId) {
		return "https://docs.google.com/spreadsheets/d/" + docId;
	}

	public String extractDriveSpreadsheetId(String adInfoSpreadsheetId) {
		if(isURL(adInfoSpreadsheetId)) {
			return textUtils.getGroup(adInfoSpreadsheetId, "\\/d\\/([^\\/]+)\\/", 1);
		} else {
			return adInfoSpreadsheetId;
		}
	}

	public List<String> lowercaseAll(List<String> tokens) {
		List<String> ntokens = new ArrayList<String>();
		for (String token : tokens) {
			ntokens.add(token.toLowerCase().trim());
		}
		return ntokens;
	}

}
