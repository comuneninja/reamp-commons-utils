package reamp.ai.data.file;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

import reamp.ai.utils.TextUtils;

public class ResourceHandler {
	private static ResourceHandler instance;
	
	private Map<String,String> resourceCache = new HashMap<String,String>();
	
	private Map<String,File> fileCache = new HashMap<String,File>();
	
	private TextUtils textUtils = TextUtils.getInstance();

	private ResourceHandler() {
	}

	public static ResourceHandler getInstance() {
		if (null == instance) {
			instance = new ResourceHandler();
		}
		return instance;
	}
	
	public File fetchBinaryResource(String templateUrl) throws Exception {
		File resourceContent = fileCache.get(templateUrl);
		if(resourceContent != null) return resourceContent;
		if(templateUrl.startsWith("file://")) {
			templateUrl = templateUrl.replaceAll("file://", "");
			File content = new File(templateUrl);
			fileCache.put(templateUrl, content);
			return content;
		} else {
			String fileLocation = "temp/" + textUtils.md5(templateUrl);
			File cacheFile = new File(fileLocation);
			FileUtils.copyURLToFile(new URL(templateUrl), cacheFile);
			fileCache.put(templateUrl, cacheFile);
			return cacheFile;
		}
	}
	
	public String fetchResource(String templateUrl) throws Exception {
		String resourceContent = resourceCache.get(templateUrl);
		if(resourceContent != null) return resourceContent;
		if(templateUrl.startsWith("file://")) {
			templateUrl = templateUrl.replaceAll("file://", "");
			String content = FileUtils.readFileToString(new File(templateUrl));
			resourceCache.put(templateUrl, content);
			return content;
		} else {
			String content = Unirest.get(templateUrl).asString().getBody();
			resourceCache.put(templateUrl, content);
			return content;
		}
	}
	
	public boolean checkResource(String templateUrl) throws Exception {
		if(templateUrl.startsWith("file://")) {
			templateUrl = templateUrl.replaceAll("file://", "");
			return new File(templateUrl).exists();
		} else {
			HttpResponse<InputStream> asBinary = Unirest.get(templateUrl).asBinary();
			int status = asBinary.getStatus();
			return status == 200;
		}
	}
}