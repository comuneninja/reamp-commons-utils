package reamp.ai.data;

import java.util.Map;

public interface MapOperator<T1, T2> {
	
	void cleanMap(Map<T1, T2> mapToClean);

}
