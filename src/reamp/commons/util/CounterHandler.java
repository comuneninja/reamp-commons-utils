package reamp.commons.util;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import com.google.common.util.concurrent.AtomicDouble;

public class CounterHandler {

	private static final String DEFAULT_SEPARATOR = " ; ";

	private Map<String, AtomicInteger> counterMap = new ConcurrentHashMap<String, AtomicInteger>();

	private Map<String, AtomicDouble> floatCounterMap = new ConcurrentHashMap<String, AtomicDouble>();

	private Map<String, Long> timerMap = new ConcurrentHashMap<String, Long>();

	private static CounterHandler INSTANCE;

	private ValueFormatter formatter = ValueFormatter.getInstance();

	public CounterHandler() {

	}

	public Map<String, AtomicInteger> getCounterMap() {
		return counterMap;
	}
	
	public Map<String, AtomicDouble> getFpCounterMap() {
		return floatCounterMap;
	}

	public void setCounterMap(Map<String, AtomicInteger> counterMap) {
		this.counterMap = counterMap;
	}

	public static CounterHandler getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new CounterHandler();
		}
		return INSTANCE;
	}

	public Integer add(String name) {
		try {
			AtomicInteger counter = counterMap.get(name);
			if (counter == null) {
				counter = new AtomicInteger(0);
				counterMap.put(name, counter);
			}
			return counter.incrementAndGet();
		} catch (Exception e) {
			return -1;
		}
	}

	public void timer(String name) {
		if (timerMap == null)
			timerMap = new HashMap<String, Long>();
		timerMap.put(name, System.currentTimeMillis());
	}

	public Long elapsed(String name) {
		Long old = timerMap.get(name);
		if (old == null)
			return null;
		return System.currentTimeMillis() - old;
	}

	public Long printElapsed(String name) {
		return printElapsed(name, System.out);
	}
	
	public Double averageTimeElapsed(String prefix) {
		return averageTimeElapsed(prefix, false);
	}
	
	public Double averageTimeElapsed(String prefix, boolean stopTimer) {
		AtomicInteger timersFound = new AtomicInteger();
		AtomicLong timeElapsed = new AtomicLong();
		
		List<String> timerKeys = new ArrayList<String>();
		
		synchronized (timerMap) {
			for (Entry<String, Long> entry : timerMap.entrySet()) {
				String timerName = entry.getKey();

				if (timerName.startsWith(prefix)) {
					timersFound.incrementAndGet();
					Long elapsed = getElapsed(timerName, stopTimer);
					if (elapsed != null) {
						timeElapsed.addAndGet(elapsed);
						timerKeys.add(timerName);
					}
				}
			}
			for (String timerKey : timerKeys) {
				timerMap.remove(timerKey);
			}
		}
		
		Double averageTimeElapsed = timeElapsed.doubleValue()/timersFound.doubleValue();
		return averageTimeElapsed;
	}
	
	public Long getElapsed(String timerName) {
		return getElapsed(timerName, false);
	}
	
	public Long getElapsed(String timerName, boolean stop) {
		Long currentTimer = timerMap.get(timerName);
		if(currentTimer == null) return null;
		Long startTime = currentTimer;
		Long currentTime = System.currentTimeMillis();
		Long elapsed = currentTime - startTime;
		return elapsed;
	}

	public Long printElapsed(String name, PrintStream out) {
		Long elapsed = elapsed(name);
		printTime(out, name, elapsed);
		return elapsed;
	}

	public Long stop(String name) {
		Long old = timerMap.get(name);
		timerMap.remove(name);
		if (old == null)
			return null;
		
		long elapsed = System.currentTimeMillis() - old;
		add(name, elapsed);
		
		return elapsed;
	}

	public Double addFp(String name, Number amount) {
		if (amount == null)
			return null;
		synchronized (floatCounterMap) {
			AtomicDouble counter = floatCounterMap.get(name);
			if (counter == null) {
				counter = new AtomicDouble(0);
				floatCounterMap.put(name, counter);
			}
			return counter.addAndGet(amount.doubleValue());
		}
	}

	public Integer add(String name, Number amount) {
		if (amount == null)
			return null;
		synchronized (counterMap) {
			AtomicInteger counter = counterMap.get(name);
			if (counter == null) {
				counter = new AtomicInteger(0);
				counterMap.put(name, counter);
			}
			return counter.addAndGet(amount.intValue());
		}
	}

	public Double getFp(String name) {
		AtomicDouble atomic;
		atomic = floatCounterMap.get(name);
		if (atomic == null)
			return 0.0;
		return atomic.doubleValue();
	}

	public Integer get(String name) {
		AtomicInteger atomicInteger;
		atomicInteger = counterMap.get(name);
		if (atomicInteger == null)
			return 0;
		return atomicInteger.intValue();
	}

	public void print(PrintStream out) {
		print(out, false);
	}
	
	public static Map<String, Integer> sort(Map<String, AtomicInteger> map) {
		Map<String,Integer> cIMap = new HashMap<String,Integer>();
		
		for (Entry<String, AtomicInteger> entry : map.entrySet()) {
			String key = entry.getKey();
			AtomicInteger value = entry.getValue();
			
			cIMap.put(key, value.intValue());
		}
		
		return sortByValue(cIMap);
	}
	
	public static Map<String, Integer> sortFloatMap(Map<String, AtomicDouble> map) {
		Map<String,Integer> cIMap = new HashMap<String,Integer>();
		
		for (Entry<String, AtomicDouble> entry : map.entrySet()) {
			String key = entry.getKey();
			AtomicDouble value = entry.getValue();
			
			cIMap.put(key, value.intValue());
		}
		
		return sortByValue(cIMap);
	}

	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			@Override
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}

	public void print(PrintStream out, boolean inline) {
		System.out.println("------------------------------------");

		if (counterMap != null && counterMap.size() > 0) {
			
			Map<String,Integer> sorted_map = sort(counterMap);

			System.out.println("COUNTERS:");
			Set<String> keySet = sorted_map.keySet();
			for (String key : keySet) {
				Integer atomicInteger = sorted_map.get(key);
				if (!inline) {
					//out.println(key + ":" + formatter.formatAsNumber(atomicInteger.intValue()));
					out.println(key + ":" + atomicInteger.intValue());
				} else {
					out.print(key + ":" + formatter.formatAsNumber(atomicInteger.intValue()) + DEFAULT_SEPARATOR);
				}
			}
		}
		
		if (floatCounterMap != null && floatCounterMap.size() > 0) {
			
			Map<String,Integer> sorted_map = sortFloatMap(floatCounterMap);

			System.out.println("COUNTERS:");
			Set<String> keySet = sorted_map.keySet();
			for (String key : keySet) {
				Integer atomicInteger = sorted_map.get(key);
				if (!inline) {
					//out.println(key + ":" + formatter.formatAsNumber(atomicInteger.intValue()));
					out.println(key + ":" + atomicInteger.intValue());
				} else {
					out.print(key + ":" + formatter.formatAsNumber(atomicInteger.intValue()) + DEFAULT_SEPARATOR);
				}
			}
		}
		
		if (timerMap != null && timerMap.size() > 0) {
			System.out.println("TIMERS:");
			for (String name : timerMap.keySet()) {
				Long elapsed = elapsed(name);
				
				printTime(out, name, elapsed, inline);
			}
		}
		System.out.println("------------------------------------");
	}

	private void printTime(PrintStream out, String name, Long elapsed) {
		printTime(out, name, elapsed, true);
	}

	private void printTime(PrintStream out, String name, Long elapsed, boolean inline) {
		String phrase = "time to " + name;
		out.println(phrase + ": " + elapsed + " (ms)");
		if (elapsed > 60000) {
			if (!inline) {
				out.println(phrase + ": " + (elapsed / 60000) + " (min)");
			} else {
				out.print(phrase + ": " + (elapsed / 60000) + " (min)" + DEFAULT_SEPARATOR);
			}
		}
		if (elapsed > 1000) {
			if (!inline) {
				out.println(phrase + ": " + (elapsed / 1000) + " (sec)");
			} else {
				out.print(phrase + ": " + (elapsed / 1000) + " (sec)" + DEFAULT_SEPARATOR);
			}
		}
	}
	
	private void printAverageTime(PrintStream out, String name, Long average, boolean inline) {
		String phrase = "average time to " + name;
		out.println(phrase + ": " + average + " (ms)");
		if (average > 60000) {
			if (!inline) {
				out.println(phrase + ": " + (average / 60000) + " (min)");
			} else {
				out.print(phrase + ": " + (average / 60000) + " (min)" + DEFAULT_SEPARATOR);
			}
		}
		if (average > 1000) {
			if (!inline) {
				out.println(phrase + ": " + (average / 1000) + " (sec)");
			} else {
				out.print(phrase + ": " + (average / 1000) + " (sec)" + DEFAULT_SEPARATOR);
			}
		}
	}

	public void addAllInList(List<String> path) {
		if(path == null) return;
		for (String s : path) {
			add(s);
		}
	}

	public void printAverageStartingWith(PrintStream out, String timerName, boolean inline) {
		Float average = averageTime(timerName);
		
		String phrase = "average time to " + timerName;
		out.println(phrase + ": " + average + " (ms)");
		if (!inline) {
			out.println(phrase + ": " + (average) + " (ms)");
		} else {
			out.print(phrase + ": " + (average) + " (ms)" + DEFAULT_SEPARATOR);
		}
	}

	private Float averageTime(String string) {
		Set<Entry<String, Long>> timerSet = timerMap.entrySet();
		
		AtomicInteger timerAmount = new AtomicInteger();
		AtomicLong totalTime = new AtomicLong();
		
		for (Entry<String, Long> entry : timerSet) {
			String key = entry.getKey();
			Long value = entry.getValue();
			
			if(!key.startsWith(string)) continue;
			
			timerAmount.incrementAndGet();
			totalTime.addAndGet(value);
		}
		
		Float average = (((float)totalTime.get())/((float)timerAmount.get()));
		return average;
	}

	public void removeTimer(String timerName) {
		timerMap.remove(timerName);
	}

}
