package reamp.commons.util;

import java.text.NumberFormat;
import java.util.Locale;

public class ValueFormatter {
	
	private static ValueFormatter INSTANCE = null;

	public static ValueFormatter getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ValueFormatter();
		}
		return INSTANCE;
	}

	private ValueFormatter() {

	}

	public String formatAsNumber(Object value) {
		String stringValue;
		Locale ptBR = new Locale("pt", "BR");
		NumberFormat numberFormat = NumberFormat.getNumberInstance(ptBR); //para números
		numberFormat.setMinimumFractionDigits(2);
		stringValue = numberFormat.format(value);
		return stringValue;
	}

}
