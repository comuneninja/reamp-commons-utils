package reamp.indexer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.google.gson.Gson;

import reamp.ai.utils.TextUtils;
import reamp.ai.utils.ValueUtils;

public class JsonUtils {

	private ValueUtils valueUtils = ValueUtils.getInstance();
	private static JsonUtils instance = new JsonUtils();
	private TextUtils textUtils = TextUtils.getInstance();
	
	private Gson gson = new Gson();
	
	private JsonUtils() {}

	public static JsonUtils getInstance() {
		return instance;
	}
	
	@SuppressWarnings("unchecked")
	public <t> List<t> convertJsonToList (String json, Class<t> clazz){
		return (List<t>) gson.fromJson(json, new ArrayList<t>().getClass());
	}
	
	public String normalizeJsonKey(String str){
		return textUtils.removeAccents(str)
				.replace("!", "_")
				.replaceAll("@", "_")
				.replaceAll("#", "_")
				.replaceAll("[\\$]", "_")
				.replaceAll("%", "per")
				.replaceAll("&", "_")
				.replaceAll("[\\*]", "_")
				.replaceAll("\\[", "_")
				.replaceAll("\\]", "_")
				.replaceAll("\\(", "_")
				.replaceAll("\\)", "_")
				.replaceAll("'", "")
				.replaceAll("\\\\", "_")
				.replaceAll("\\/", "_")
				.replaceAll("\\?", "_")
				.replaceAll("\\;", "_")
				.replaceAll("\\:", "_")
				.replaceAll("\\.", "_")
				.replaceAll("\\,", "_")
				.replaceAll("\\{", "_")
				.replaceAll("\\}", "_")
				.replaceAll("-", "_")
				.replaceAll(" ", "_")
				.replaceAll("\"", "")
				.replaceAll("_$", "")
				.replaceAll("[\\_]{2,}", "_").toLowerCase();
	}
	
	public JsonArray createJsonArrayFromMapList(List<Map<String, String>> genericMapList){
		JsonArray arrData = new JsonArray();
		
		for (Map<String, String> map : genericMapList) {
			
			JsonObject line = new JsonObject();
			
			for (Entry<String, String> entry : map.entrySet()) {
				String key = normalizeJsonKey(entry.getKey()); 
				String value = entry.getValue();
				Double dob = valueUtils.forceParseAsDouble(value);

				if (dob != null) {
					line.add(key, dob);
					continue;
				}

				line.add(key, value);
			}

			arrData.add(line);
		}
		
		return arrData;
	}
	
}
