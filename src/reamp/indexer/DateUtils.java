package reamp.indexer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import reamp.ai.utils.ValueUtils;

public class DateUtils {

	private static final DateUtils singleton = new DateUtils();

	private ValueUtils valueUtils = ValueUtils.getInstance();

	private static final SimpleDateFormat day = new SimpleDateFormat("yyyyMMdd");
	private static final SimpleDateFormat hour = new SimpleDateFormat("yyyyMMddHH");
	private static final SimpleDateFormat minute = new SimpleDateFormat("yyyyMMddHHmm");
	private static final SimpleDateFormat month = new SimpleDateFormat("yyyyMM");
	private static final SimpleDateFormat mysqlDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat spreadsheetDate = new SimpleDateFormat("MM/dd/yyyy");
	private static final SimpleDateFormat onlyHour = new SimpleDateFormat("HH:mm:ss");
	private static final SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat dayAndMonth = new SimpleDateFormat("dd-MM");
	private static final SimpleDateFormat brFormat = new SimpleDateFormat("dd/MM/yyyy");

	final String[] possibleDateFormats = { "yyyy-MM", "yyyy-MM-dd", "yyyyMMddHHmmss", "yyyyMMddHHmm", "yyyyMMddHH", "yyyyMMdd", "yyyyMM", "dd/MM/yyyy", "dd/MM", "dd-MM", "yyyy" };

	public static DateUtils getInstance() {
		return singleton;
	}

	private DateUtils() {
	}

	public String simpleFormat(Date d, int dateField) {
		switch (dateField) {
		case Calendar.MONTH:
			return month.format(d);
		case Calendar.DAY_OF_MONTH:
			return day.format(d);
		case Calendar.HOUR:
			return hour.format(d);
		case Calendar.MINUTE:
			return minute.format(d);
		default:
			break;
		}

		return null;
	}

	public Date parseFlexibly(String s) {
		String[] pdf = possibleDateFormats;
		return parseFlexibly(s, false, pdf);
	}

	public Date parseFlexibly(String s, boolean future) {
		String[] pdf = possibleDateFormats;
		return parseFlexibly(s, future, pdf);
	}

	public static void main(String[] args) {
		System.out.println(DateUtils.getInstance().parseFlexibly("0705", true, "dd/MM"));
	}

	public Date parseFlexibly(String s, String... pdf) {
		return parseFlexibly(s, false, pdf);
	}

	public Date parseFlexibly(String s, boolean future, String... pdf) {
		if (pdf == null || pdf.length == 0)
			return null;

		if (s.startsWith("14") && s.length() > 9 && !s.contains("-") && !s.contains("/")) {
			// timestamp
			return asTimestamp(s);
		}

		for (String df : pdf) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(df);
				Date parse = sdf.parse(s);
				if (future) {
					Calendar c = Calendar.getInstance();
					c.setTime(parse);
					Calendar ref = Calendar.getInstance();
					ref.setTime(new Date());

					int currentYear = ref.get(Calendar.YEAR);
					int calendarYear = c.get(Calendar.YEAR);
					if (calendarYear < currentYear)
						c.set(Calendar.YEAR, currentYear);

					return c.getTime();
				}
				return parse;
			} catch (Exception e) {
				continue;
			}
		}
		return null;
	}

	public String toName(Date d) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("w-yyyy");
			return sdf.format(d);
		} catch (Exception e) {
			return null;
		}
	}

	public String toString(Date d) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			return sdf.format(d);
		} catch (Exception e) {
			return null;
		}
	}

	public Date asTimestamp(String fieldValue) {
		Date d = null;
		try {
			d = new Date(Long.parseLong(fieldValue + "000"));
		} catch (Exception e) {
			System.out.println("error parsing date field value " + fieldValue);
			return null;
		}
		return d;
	}

	public String formattedNow() {
		return toString(new Date());
	}

	public String formattedForInsertion(Date d) {
		try {
			return mysqlDate.format(d);
		} catch (Exception e) {
			Date parsed = parseFlexibly(d.toString());
			return mysqlDate.format(parsed);
		}
	}

	public String formattedForES(String d) {
		if (d == null || d.length() == 0)
			return null;
		return formattedForES(parseFlexibly(d));
	}

	public String formattedForES(Date d) {
		try {
			return simpleFormat.format(d);
		} catch (Exception e) {
			Date parsed = parseFlexibly(d.toString());
			return simpleFormat.format(parsed);
		}
	}

	public String simpleFormat(Date d) {
		try {

			return simpleFormat.format(d);
		} catch (Exception e) {
			Date parsed = parseFlexibly(d.toString());
			return simpleFormat.format(parsed);
		}
	}

	public String firstDate(List<Date> dates) {
		Collections.sort(dates, new Comparator<Date>() {
			public int compare(Date a, Date b) {
				return a.compareTo(b);
			}
		});

		if (dates.size() > 0) {
			Date first = dates.get(0);

			return toString(first);
		}

		return null;
	}

	public String lastDate(List<Date> dates) {
		Collections.sort(dates, new Comparator<Date>() {
			public int compare(Date a, Date b) {
				return a.compareTo(b);
			}
		});

		if (dates.size() > 0) {
			Date last = dates.get(dates.size() - 1);

			return toString(last);
		}

		return null;
	}

	public Integer daysBetween(Date date1, Date date2) {
		try {
			long diff = date2.getTime() - date1.getTime();
			int days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			return days;
		} catch (Exception e) {
			return -1;
		}
	}

	public Date dayAfterTomorrow() {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DAY_OF_YEAR, 2);
		return c.getTime();
	}

	public Date yesterday() {
		int shift = -1;
		return shiftDays(shift);
	}

	public Date shiftDays(int shift) {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DAY_OF_YEAR, shift);
		return c.getTime();
	}

	public Date shiftDays(Date date, int shift) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DAY_OF_YEAR, shift);
		return c.getTime();
	}

	public Date startOfDay(Date d) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		return c.getTime();
	}

	public Date parseMysqlStringToDate(String source) throws ParseException {
		return mysqlDate.parse(source);
	}

	public String formatTime(long seconds) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(seconds * 1000);
		c.add(Calendar.HOUR, 3);
		Date d = c.getTime();
		return onlyHour.format(d);
	}

	public String secondsInHours(long seconds) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(seconds * 1000);
		Date d = c.getTime();
		return onlyHour.format(d);
	}

	public Date firstDayOfMonth(Date d) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(Calendar.DAY_OF_MONTH, 1);
		return c.getTime();
	}

	public String formatOnlyDayAndMonth(Date date) {
		return dayAndMonth.format(date);
	}

	public Date currentDateInUtc(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR, 2);
		return c.getTime();
	}

	public String formattedForSpreadsheet(Date d) {
		try {
			return spreadsheetDate.format(d);
		} catch (Exception e) {
			Date parsed = parseFlexibly(d.toString());
			return spreadsheetDate.format(parsed);
		}
	}

	public Date parseSimpleFormat(String format) {
		try {
			return simpleFormat.parse(format);
		} catch (ParseException e) {
			return null;
		}
	}

	public Date dateAndHour(Date date, int hour) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		return cal.getTime();

	}

	public Date truncate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public Date createDateFromXlsFormat(Integer date) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.YEAR, 1899);
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.DAY_OF_MONTH, 30);
		cal.add(Calendar.DATE, date);
		return cal.getTime();
	}

	public Date createDateFromXlsFormat(String sdate) {
		try {
			return createDateFromXlsFormat(valueUtils.forceParseAsInt(sdate));
		} catch (Exception ex) {
			return null;
		}
	}

	public String brazilianFormat(Date d) {
		return brFormat.format(d);
	}

	public Date smallestDate(Date d, Date date) {
		Date smallestDate = null;

		if (d.getTime() < date.getTime())
			smallestDate = d;
		else
			smallestDate = date;

		return smallestDate;
	}

	public Date highestDate(Date d, Date date) {
		Date highestDate = null;

		if (d.getTime() > date.getTime())
			highestDate = d;
		else
			highestDate = date;

		return highestDate;
	}

	public Date parse(String date, String dateFormart) {
		SimpleDateFormat format = new SimpleDateFormat(dateFormart);
		try {
			return format.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String parseToString(Date date, String dateFormart) {
		SimpleDateFormat format = new SimpleDateFormat(dateFormart);
		try {
			return format.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public Date oneMonthAgo() {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.MONTH, -1);
		return c.getTime();
	}
}
