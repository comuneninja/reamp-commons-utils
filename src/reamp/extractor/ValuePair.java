package reamp.extractor;

public class ValuePair<V1,V2> {
	
	private V1 key;
	
	private V2 value;

	public ValuePair(V1 key, V2 value) {
		super();
		this.key = key;
		this.value = value;
	}

	public V1 getKey() {
		return key;
	}

	public void setKey(V1 key) {
		this.key = key;
	}

	public V2 getValue() {
		return value;
	}

	public void setValue(V2 value) {
		this.value = value;
	}


}
