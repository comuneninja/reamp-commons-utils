package reamp.extractor;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class ListUtils {

	private static final String COUNTER_SEPARATOR = "-xxx-";
	private static final ListUtils singleton = new ListUtils();

	public static ListUtils getInstance() {
		return singleton;
	}

	protected ListUtils() {
	}

	public <T> T randomItem(List<T> list) {
		if (list == null || list.size() == 0)
			return null;
		return list.get(((int) (Math.random() * (list.size() - 1))));
	}

	public <T> List<T> addToList(List<T> listA, T e, int maxSize) {
		if (listA == null)
			return null;
		if (maxSize <= 0)
			return listA;
		List<T> nL = new ArrayList<T>();
		int startIndex = listA.size() - maxSize;
		if (startIndex > 0) {
			// ja tem mais do que deveria
			for (int s = startIndex + 1; s < listA.size(); s++) {
				T t = listA.get(s);
				nL.add(t);
			}
		} else {
			listA.add(e);
			return listA;
		}
		nL.add(e);

		return nL;
	}

	public <T> List<T> addToListIfLastNotSameAndLimitSize(List<T> listToAddTo, T elementToBeAdd, int listMaxSize) {
		if (listToAddTo == null)
			return null;
		if (listMaxSize <= 0)
			return listToAddTo;
		List<T> nL = new ArrayList<T>();
		int startIndex = listToAddTo.size() - listMaxSize;
		if (startIndex > 0) {
			// ja tem mais do que deveria
			for (int s = startIndex + 1; s < listToAddTo.size(); s++) {
				T t = listToAddTo.get(s);
				nL.add(t);
			}
		} else {
			addToListIfLastNotSame(listToAddTo, elementToBeAdd);
			return listToAddTo;
		}
		addToListIfLastNotSame(nL, elementToBeAdd);

		return nL;
	}

	public <T> List<T> addToListAsASet(List<T> listToAddTo, List<T> elementToBeAdd, int listMaxSize) {
		for (T t : elementToBeAdd) {
			listToAddTo = addToListAsASet(listToAddTo, t, listMaxSize);
		}
		return listToAddTo;
	}

	public List<String> addToListAsASetAndSum(List<String> listToAddTo, List<String> elementToBeAdd, int listMaxSize) {
		for (String e : elementToBeAdd) {
			addToListAsASetAndSum(listToAddTo, e, listMaxSize);
		}
		return listToAddTo;
	}

	public List<String> addToListAsASetAndSum(List<String> listToAddTo, String elementToBeAdd, int listMaxSize) {
		if (listToAddTo == null)
			return null;
		if (listMaxSize <= 0)
			return listToAddTo;
		int remainingSize = listMaxSize - listToAddTo.size();
		// ja tem mais do que deveria

		// vamos apenas pegar o ultimo
		String lastString = listToAddTo.size() == 0 ? null : listToAddTo.get(listToAddTo.size() - 1);
		String lastStringName = lastString;
		if (lastString.contains(COUNTER_SEPARATOR)) {
			String[] stringValueSplit = lastString.split(COUNTER_SEPARATOR);
			lastStringName = stringValueSplit[1];
		}

		if (elementToBeAdd.equals(lastStringName)) {
			Integer currentCount = 1;
			String elementName = lastString;
			if (lastString.contains(COUNTER_SEPARATOR)) {
				String[] stringValueSplit = lastString.split(COUNTER_SEPARATOR);
				currentCount = Integer.parseInt(stringValueSplit[0]);
				elementName = stringValueSplit[1];
			}
			String finalString = ++currentCount + COUNTER_SEPARATOR + elementName;

			// remove last
			listToAddTo.remove(listToAddTo.size() - 1);
			listToAddTo.add(finalString);
		} else {
			listToAddTo.add(elementToBeAdd);
		}

		int remove = 0;
		while (remainingSize < 0) {
			listToAddTo.remove(remove++);
			remainingSize = listMaxSize - listToAddTo.size();
		}

		return listToAddTo;
	}

	public <T> List<T> addToListAsASet(List<T> listToAddTo, T elementToBeAdd, int listMaxSize) {
		if (listToAddTo == null)
			return null;
		if (listMaxSize <= 0)
			return listToAddTo;
		List<T> nL = new ArrayList<T>();
		int startIndex = listToAddTo.size() - listMaxSize;
		if (startIndex > 0) {
			// ja tem mais do que deveria
			for (int s = startIndex + 1; s < listToAddTo.size(); s++) {
				T t = listToAddTo.get(s);
				if (t != null && t.equals(elementToBeAdd))
					continue;
				nL.add(t);
			}
		} else {
			for (int s = 0; s < listToAddTo.size(); s++) {
				T t = listToAddTo.get(s);
				if (t != null && t.equals(elementToBeAdd))
					continue;
				nL.add(t);
			}
		}
		nL.add(elementToBeAdd);

		return nL;
	}

	private <T> void addToListIfLastNotSame(List<T> listA, T e) {
		T last = listA.get(listA.size() - 1);
		if (last != null && last != e && !last.equals(e))
			listA.add(e);
	}
	
	public String printList(List<String> l) {
		StringBuilder sb = new StringBuilder();
		for (String s : l) {
			sb.append(s);
		}
		return sb.toString();
	}

}
