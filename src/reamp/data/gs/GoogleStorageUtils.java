package reamp.data.gs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.api.gax.paging.Page;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

public class GoogleStorageUtils {

	private static GoogleStorageUtils instance;

	private Storage storage;

	public static void main(String[] args) throws IOException {
		GoogleStorageUtils s = GoogleStorageUtils.getInstance();
		//s.uploadFile(new File("pom.xml"), "reamp-assets");
		s.listBuckets();
	}

	private GoogleStorageUtils() {
		init();
	}

	private void init() {
		try {
			this.storage = StorageOptions.getDefaultInstance().getService();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static GoogleStorageUtils getInstance() {
		if (null == instance) {
			instance = new GoogleStorageUtils();
		}
		return instance;
	}

	public void listBuckets() {
		System.out.println("Buckets:");
		Page<Bucket> buckets = storage.list();
		for (Bucket bucket : buckets.iterateAll()) {
			System.out.println(bucket.toString());
		}
	}
	
	public String uploadFile(java.io.File file, final String bucketName) throws IOException {
		return uploadFile(file, bucketName, "inline");
	}

	/**
	 * Uploads a file to Google Cloud Storage to the bucket specified in the
	 * BUCKET_NAME environment variable, appending a timestamp to end of the
	 * uploaded filename.
	 */
	// Note: this sample assumes small files are uploaded. For large files or
	// streams use:
	// Storage.writer(BlobInfo blobInfo, Storage.BlobWriteOption... options)
	public String uploadFile(java.io.File file, final String bucketName, String contentDisposition) throws IOException {
		DateTimeFormatter dtf = DateTimeFormat.forPattern("-YYYY-MM-dd-HHmmssSSS");
		DateTime dt = DateTime.now(DateTimeZone.UTC);
		String dtString = dt.toString(dtf);
		final String fileName = file.getName();

		// The InputStream is closed by default, so we don't need to close it here
		// Read InputStream into a ByteArrayOutputStream.
		InputStream is = new FileInputStream(file);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		byte[] readBuf = new byte[4096];
		while (is.available() > 0) {
			int bytesRead = is.read(readBuf);
			os.write(readBuf, 0, bytesRead);
		}

		// Convert ByteArrayOutputStream into byte[]
		BlobInfo blobInfo = storage.create(BlobInfo.newBuilder(bucketName, fileName)
				// Modify access list to allow all users with link to read file
				.setContentType("text/html")
				.setContentDisposition(contentDisposition)
				.build(),
				os.toByteArray());
		// return the public download link
		return blobInfo.getMediaLink();
	}

	public String putObject(String contentStr, String fileName, String contentDisposition, String bucketName) {
		File file = new File(fileName);
		try {
			FileUtils.writeStringToFile(file, contentStr);
			String fileUrl = uploadFile(file, bucketName);
			return fileUrl;
		} catch (Exception e) {
			e.printStackTrace();
			// sei la .. n cosneguiu gravar
			return null;
		}
	}
}
