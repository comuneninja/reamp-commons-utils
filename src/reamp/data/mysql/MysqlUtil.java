package reamp.data.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MysqlUtil {
	
	private Connection connect;
	
	private static MysqlUtil instance;

	private MysqlUtil() {
	}

	public static MysqlUtil getInstance() {
		if (null == instance) {
			instance = new MysqlUtil();
		}
		return instance;
	}
	
	private void init() {
		try {
			connect = DriverManager.getConnection(
					"jdbc:mysql://130.211.114.26:3306/vitrines?"
							+ "user=root&password=VFR$5tgb");
			// initCampaignData();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void insertDeliveryLog(String date, String campaignId, String zoneId, String views, String clicks) {
			if (connect == null)
				init();
			
			String sql = "insert into delivery_logs (`date`, `campaign_id`, `zone_id`, `views`, `clicks`, "
					+ "`bm_value`) values ("
					+ "'" + date + "'," + campaignId + "," + zoneId + "," + views + "," + clicks + "," + "0.01)";

			System.out.println(sql);

			try {
				PreparedStatement preparedStatement = null;

				preparedStatement = connect.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

				preparedStatement.executeUpdate();
				ResultSet rs = preparedStatement.getGeneratedKeys();
				rs.next();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
			} finally {
			}
	}

}
