package reamp.data.bq;

public class DeliveryLog {
	
	private String date;
	private String campaignID;
	private String zoneID;
	private String clicks;
	private String impressions;
	
	public DeliveryLog(String date, String campaignID, String zoneID, String clicks, String impressions) {
		super();
		this.date = date;
		this.campaignID = campaignID;
		this.zoneID = zoneID;
		this.clicks = clicks;
		this.impressions = impressions;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCampaignID() {
		return campaignID;
	}
	public void setCampaignID(String campaignID) {
		this.campaignID = campaignID;
	}
	public String getZoneID() {
		return zoneID;
	}
	public void setZoneID(String zoneID) {
		this.zoneID = zoneID;
	}
	public String getClicks() {
		return clicks;
	}
	public void setClicks(String clicks) {
		this.clicks = clicks;
	}
	public String getImpressions() {
		return impressions;
	}
	public void setImpressions(String impressions) {
		this.impressions = impressions;
	}
}
