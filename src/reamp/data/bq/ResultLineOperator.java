package reamp.data.bq;

import com.google.cloud.bigquery.FieldValueList;

public interface ResultLineOperator {
	
	public void operateOn(FieldValueList fvl);

}
