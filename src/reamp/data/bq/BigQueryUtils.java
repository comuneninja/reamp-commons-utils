package reamp.data.bq;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryError;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.BigQueryOptions.Builder;
import com.google.cloud.bigquery.Field;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.InsertAllRequest;
import com.google.cloud.bigquery.InsertAllResponse;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobId;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.LegacySQLTypeName;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.Schema;
import com.google.cloud.bigquery.StandardTableDefinition;
import com.google.cloud.bigquery.TableDefinition;
import com.google.cloud.bigquery.TableId;
import com.google.cloud.bigquery.TableInfo;
import com.google.cloud.bigquery.TableResult;
import com.google.cloud.http.HttpTransportOptions;

public class BigQueryUtils {

	private static final String DEFAULT_PROJECT_ID = "reamp-adserver";
	private static final int MAXTIME_FLUSH = 5000;
	private static BigQueryUtils instance;
	private static BigQuery bigquery;
	
	private Map<String, TableId> tableMap = new HashMap<String,TableId>();
	private Map<String, InsertAllRequest.Builder> builderMap = new HashMap<String, InsertAllRequest.Builder>();
	private Map<String, AtomicLong> batchedRowsMap = new HashMap<String, AtomicLong>();
	
	public void initTable(String table) {
		String[] split = table.split("_");
		String nameSpace = split[0];
		String dataSet = split[1];
		String tableName = split[2];
		
		TableId tableId = TableId.of(nameSpace, dataSet, tableName);
		InsertAllRequest.Builder builder = InsertAllRequest.newBuilder(tableId);
		
		tableMap.put(table, tableId);
		builderMap.put(table, builder);
		batchedRowsMap.put(table, new AtomicLong(0));
	}
	
	public Iterable<FieldValueList> queryAndOperate(String query, ResultLineOperator lineOperator) throws Exception {
		//initTable(table);
		
		System.out.println(query);
		
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(
		            query)
		        .setUseLegacySql(false)
		        .build();

		// Create a job ID so that we can safely retry.
		JobId jobId = JobId.of(UUID.randomUUID().toString());
		Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());

		// Wait for the query to complete.
		queryJob = queryJob.waitFor();

		// Check for errors
		if (queryJob == null) {
		  throw new RuntimeException("Job no longer exists");
		} else if (queryJob.getStatus().getError() != null) {
		  // You can also look at queryJob.getStatus().getExecutionErrors() for all
		  // errors, not just the latest one.
		  throw new RuntimeException(queryJob.getStatus().getError().toString());
		}
		
		TableResult queryResults = queryJob.getQueryResults();
		
		Iterable<FieldValueList> values = queryResults.iterateAll();
		if(lineOperator != null && values != null)
		for (FieldValueList fvl : values) {
			lineOperator.operateOn(fvl);
		}
		return values;
	}
	
	Thread flusher = new Thread() {
		public void run() {
			while (true) {
				try {
					for (Entry<String, InsertAllRequest.Builder> entry : builderMap.entrySet()) {
						String key = entry.getKey();
						InsertAllRequest.Builder value = entry.getValue();
						
						TableId tableId = tableMap.get(key);
						
						synchronized (value) {
							flush(key, tableId, value);
						}
					}
					Thread.sleep(MAXTIME_FLUSH);
				} catch (Exception e) {
					continue;
				}
			}
		}
	};
	
	private BigQueryUtils() {
		this("reamp-adserver", "{\n" + 
				"  \"type\": \"service_account\",\n" + 
				"  \"project_id\": \"reamp-adserver\",\n" + 
				"  \"private_key_id\": \"adc7a7dc9d9473acc2ae77ae078af651162e75a2\",\n" + 
				"  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDRKpZodeEhtgZ6\\nGCw9ADEt1Fr01+DmPRYkc3eChB0cqbNqBVNajZDLZDw7zbzKFv3HgTmNemoHcPxE\\nv1F8ZAUDGfaYaxum7kArnN85HAJLqehAPffjyArRHtoU937ju6EaJJNbmpY1LGDm\\n1lxQnordXjUeJy7urIa04wq37xG0cuoARYEEBbXOf/OGMKz4O1IF7HB50ck8jcgS\\npabf54xqhKX6dUOoUZnj1hdpFEC7BhDOWM0VzKKOzqrwIBwStC7HZvQBzPBv+KsO\\nSd9n4PdH6dFUh3XrclM5VgH6hE/AKztMK0fp60DPj6sWEct9KMKBA9xxM/pA05ag\\n3czVUoMXAgMBAAECggEAAOZpkt90S6ktErhV5MFVja1Ejtbn1g7eDiECaWFi20Ag\\n+V7a1uKEewYzYvzpE3vXxMctABT6TGAQK687hJV8ynOl5yQiXttct6uvKRyrJyTP\\nPbNJ6B7ULWKuP/1HmuGjBqFkbBH5FFhoAwbBGLcWDaHg5krU6p9NoAss9ZRcuY0/\\n9iiBLxhiieYijtI09pBIp/Jg/CV1Kt/B+pyBqieScXbH8DwZHyGVaX0/bZykzQDW\\n2lt8aKJUBSYaEAznaRJHKvqIUuUk7zKzLhAyqRDSFKikYIeobmXvU86WGI3caaiz\\ntkgQrynfafEVMe506o5HPtvOaE1WIKforO9/sk31MQKBgQD+aNuvqQGXQRoEN3f/\\nlt+xoiSVVVRFKHSGtQk9zboDKkFf8egq5jNJY5wQXBroJlxaaJvHdnISzMKNBHHL\\n8nOsK3n/BeddQhAruPesDnClj3LP+qAYJ/3OQhjIY5hbBD8rKZ2Ik4+4gJ0CkOLu\\nDtx5gUrjsV2ZQvkI4zsfTISK8QKBgQDSeVMmxaa6HueqB/V788KWrQWEpMvh7cTX\\n8fphCFRoQrVC2UCphEjW0Bg+Z0lpS5obHiEoVY30coGR8a+u4Cg659Pf1QNlJBeP\\n2rgu9tAlgkNuw983zXzbc9h5rVf4jprw2F2ZO75Cybl1lW0LMBE+QlDbfmQ9KWTZ\\n+QLZo3UehwKBgQCboVO6up2RhK7Ao5oRzZ7cEH9mpwNdJylZeofDugUviHrjkZ1g\\nGOCI5ZmD3IRNPaJDYDjjpQ6Vd5MQJwbiJ1ghm5sOqfLl9tLjnExOgarhbMZkekHT\\nZSt7MK3ucBa0nv1xjwyijvpdeLgT6MQZsxemcLR6Y3cJmZG0oST6uZlfUQKBgDdQ\\nu5HNEGyKAsilHtdc0R9YicK6rPrLypgWeH9X+RcNsSXIgz4KDjqmpjiAXMLXo8DM\\nwlofYheEoJkCs/F56BjTJNQG38WNHsQn75AoPbwd6CsMP3YjA1WJtLMF48DnecEi\\nnDbRhMd4zM0iYG44S8/ZDfch2KZaYq8GysD4PKexAoGAB8C5HC77/nAo3O2dyjRI\\n8pjB5PhyNPsywQZuGkTblPdJdkvlr0Vp2P/xOJTK0VLfrtKKEeirNZ/HepK7yBt7\\nFFVQKSEtrzGfToXEZPl+zmaMKF2PzB9SLE7JDRnQZl4gYNud4QXKjOYDuYucSMsk\\nKQL+xx0YAoxTNY4pwPcOR8I=\\n-----END PRIVATE KEY-----\\n\",\n" + 
				"  \"client_email\": \"bigquery-main@reamp-adserver.iam.gserviceaccount.com\",\n" + 
				"  \"client_id\": \"111858066577954129350\",\n" + 
				"  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" + 
				"  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" + 
				"  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" + 
				"  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/bigquery-main%40reamp-adserver.iam.gserviceaccount.com\"\n" + 
				"}\n" + 
				"");
	}

	public BigQueryUtils(String projectId, String credentialsString) {
		HttpTransportOptions transportOptions = BigQueryOptions.getDefaultHttpTransportOptions().toBuilder()
				.setConnectTimeout(0).build();

		try {
			GoogleCredentials credentials;
			
			try (InputStream serviceAccountStream = credentialsInputStream(credentialsString)) {
				credentials = ServiceAccountCredentials.fromStream(serviceAccountStream);
			}
			Builder cred = BigQueryOptions.newBuilder();
			cred.setProjectId(projectId == null ? DEFAULT_PROJECT_ID : projectId);
			cred.setCredentials(credentials);
			cred.setTransportOptions(transportOptions);
			BigQueryOptions build = cred.build();
			bigquery = build.getService();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		flusher.start();
	}
	
	private InputStream credentialsInputStream(String credentialsString) throws Exception {
		InputStream stream = new ByteArrayInputStream(
				(credentialsString)
				.getBytes(StandardCharsets.UTF_8));
		return stream;
	}
	
	public static BigQueryUtils getInstance() {
		return getInstance(null, null);
	}

	public static BigQueryUtils getInstance(String projectId, String accessString) {
		if (null == instance) {
			if(accessString != null) {
				instance = new BigQueryUtils(projectId, accessString);
			} else {
				instance = new BigQueryUtils();
			}
		}
		return instance;
	}
	
	public void deleteTable(String table) throws Exception {
		TableId tableId = tableMap.get(table);
		if(tableId == null) {
			initTable(table);
			tableId = tableMap.get(table);
		}
		bigquery.delete(tableId);
	}
	
	public void createTable(String tableName, String... fields) throws Exception {
		List<Field> fieldList = new ArrayList<Field>();
		for (String field : fields) {
			Field f = field(field);
			fieldList.add(f);
		}
		
		Schema schema = Schema.of(fieldList.toArray(new Field[] {}));
		
		TableDefinition tableDefinition = StandardTableDefinition.of(schema);
		
		String projectId = tableName.split("_")[0];
		String dataSet = tableName.split("_")[1];
		String table = tableName.split("_")[2];
		
		TableId tableId = TableId.of(projectId, dataSet, table);
		TableInfo tableInfo = TableInfo.newBuilder(tableId, tableDefinition).build();
		bigquery.create(tableInfo);
	}
	
	private Field field(String name) {
		int lastUnderline = name.lastIndexOf("_");
		
		String fieldName = name;
		String typeStr = name.substring(lastUnderline + 1); 
		
		LegacySQLTypeName type = LegacySQLTypeName.STRING;
		if(typeStr != null && typeStr.equals("f")) {
			fieldName = name.substring(0,lastUnderline);
			type = LegacySQLTypeName.FLOAT;
		} else 
		if(typeStr != null && typeStr.equals("i")) {
			fieldName = name.substring(0,lastUnderline);
			type = LegacySQLTypeName.INTEGER;
		} else 
		if(typeStr != null && typeStr.equals("dt")) {
			fieldName = name.substring(0,lastUnderline);
			type = LegacySQLTypeName.DATETIME;
		} else 
		if(typeStr != null && typeStr.equals("at")) {
			fieldName = name.substring(0,lastUnderline);
			type = LegacySQLTypeName.TIMESTAMP;
		} else
		if(typeStr != null && typeStr.equals("d")) {
			fieldName = name.substring(0,lastUnderline);
			type = LegacySQLTypeName.DATE;
		} else
		if(typeStr != null && typeStr.equals("b")) {
			fieldName = name.substring(0,lastUnderline);
			type = LegacySQLTypeName.BOOLEAN;
		}
		
		return Field.of(fieldName, type);
	}

	private static final int maxBatchedRows = 30000;
	
	public void insert(String table, Map<String, Object>... values) {
		
		TableId tableId = tableMap.get(table);

		InsertAllRequest.Builder builderRows = InsertAllRequest.newBuilder(tableId);

		Arrays.asList(values).stream().forEach(m -> builderRows.addRow(m));

		InsertAllResponse response = bigquery.insertAll(builderRows.build());

		if (response.hasErrors()) {
			for (Entry<Long, List<BigQueryError>> entry : response.getInsertErrors().entrySet()) {
				System.out.println(entry.getValue());
			}
		}
	}
	
	public void append(String table, Map<String, Object> rowContent) {
		System.out.println("appending " + rowContent);
		
		TableId tableId = tableMap.get(table);
		
		if(tableId == null) {
			try {
				initTable(table);
				tableId = tableMap.get(table);
			} catch (Exception e) {
				return;
			}
		}
		
		InsertAllRequest.Builder builder = builderMap.get(table);
		
		synchronized (builder) {
			builder.addRow("rowId" + System.currentTimeMillis() + Math.random() * 9999999, rowContent);
			AtomicLong batchedRows = batchedRowsMap.get(table);
			batchedRows.addAndGet(1);
			if (batchedRows.get() > maxBatchedRows) {
				flush(table, tableId,builder);
			}
		}
	}

	public void flush(String table, TableId tableId, InsertAllRequest.Builder addRow) {
		final InsertAllRequest rowBuilt = addRow.build();
		addRow = InsertAllRequest.newBuilder(tableId);
		batchedRowsMap.put(table, new AtomicLong(0));
		new Thread() {
			public void run() {
				try {
					InsertAllResponse response = bigquery.insertAll(rowBuilt);
					if (response.hasErrors()) {
						for (Entry<Long, List<BigQueryError>> entry : response.getInsertErrors().entrySet()) {
							System.out.println(entry.toString());
						}
					}
				} catch (Exception e) {
					return;
				}
			}
		}.start();
	}

}
